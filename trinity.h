#ifndef TRINITY_H
#define TRINITY_H

#include <exec/types.h>

#define TRINITY_DMA_COUNT 4	// NOTE: may not be all implemented

typedef struct
{
	struct tnt_i2c // offset $00
	{
		volatile unsigned int CON;
		volatile unsigned int STA;
		volatile unsigned int DAT;
		volatile unsigned int DIV;
	} i2c;
	
	struct tnt_spi	// offset $10
	{
		volatile unsigned int CON;
		volatile unsigned int STA;
		volatile unsigned int DAT;
		volatile unsigned int DIV;
	} spi;
	
	struct tnt_i2s	// offset $20
	{
		volatile unsigned int CON;
		volatile unsigned int STA;
		volatile unsigned int DAT;
		volatile unsigned int Reserved;
	} i2s;
	
	unsigned int gcon;
	unsigned int Reserved34[3];

	struct tnt_pllm // offset $40
	{
		volatile unsigned int ClkOut[6];
		volatile unsigned int ClkFBOut;
		volatile unsigned int DivClk;
		volatile unsigned int LockH;
		volatile unsigned int LockL;
		volatile unsigned int Filter;
		volatile unsigned int Control;
		volatile unsigned int Reserved[4];
	} pllm;	

	struct tnt_dma	// offset $80
	{
		volatile unsigned int CON;
		volatile unsigned int SRC;
		volatile unsigned int PTR;
		volatile unsigned int LEN;
	} dma[TRINITY_DMA_COUNT];
	
	struct tnt_psg	// offset $C0 FIXME
	{
		volatile unsigned int REG[16];
	} psg;

} tnt_control_t;

#define TNT_GCONF_KICKCDIS	(1<<1)
#define TNT_GCONF_CHIPCDIS	(1<<0)


typedef struct
{
	unsigned char res0[3];
	unsigned char PER_CHAL;
	unsigned char res1[3];
	unsigned char PER_CHAH;
	unsigned char res2[3];
	unsigned char PER_CHBL;
	unsigned char res3[3];
	unsigned char PER_CHBH;
	unsigned char res4[3];
	unsigned char PER_CHCL;
	unsigned char res5[3];
	unsigned char PER_CHCH;
	unsigned char res6[3];
	unsigned char PER_NOISE;
	unsigned char res7[3];
	unsigned char CHSEL;
	unsigned char res8[3];
	unsigned char VOL_CHA;
	unsigned char res9[3];
	unsigned char VOL_CHB;
	unsigned char res10[3];
	unsigned char VOL_CHC;
	unsigned char res11[3];
	unsigned char PER_ENVL;
	unsigned char res12[3];
	unsigned char PER_ENVH;
	unsigned char res13[3];
	unsigned char ENV_SHAPE;
} tnt_psg_regs_t;




tnt_control_t *trinity_find();


#endif // TRINITY_H

