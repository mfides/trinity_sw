/*
 * Musicport ahi driver library
 * version 2.1 22-nov-2022 Miguel Fides
 */

#include <exec/types.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <exec/errors.h>
#include <exec/ports.h>
#include <libraries/dos.h>
#include <dos/dostags.h>

#pragma dontwarn 61
#include <devices/ahi.h>
#include <libraries/ahi_sub.h>
#pragma popwarn

#include "mp_ahi.h"
#include "musicport.h"

#ifdef DEBUG
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>

static int _debug(const char *format, ...);

#define _DEBUG(...) _debug(__VA_ARGS__)
#else
#define _DEBUG(...) { /* nothing */ }
#endif

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>


#ifdef DEBUG

static BPTR _conout = 0;

struct data_context
{
	BPTR output;
	int done;
};

static void __saveds _putchproc(__reg("d0") c, __reg("a3") data)
{
	struct data_context *dc = (struct data_context *)data;
	FPutC(dc->output, c);
	dc->done++;
}

static int _vfprintf(BPTR stream, const char *format, va_list args)
{
	struct data_context data;
	data.output = stream;
	data.done = 0;
	RawDoFmt(format, args, _putchproc, &data);
	return data.done;
}

static int _vdebug(const char *format, va_list args)
{
	if (_conout == 0)
	{
		_conout = Open("CON:////Musicport Debug", MODE_NEWFILE);
	}
	
	if (_conout != 0)
	{
		int done = _vfprintf(_conout, format, args);
		Flush(_conout);
		return done;
	}
	return -1;	
}

static int _debug(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	return _vdebug(format, args);
}

#endif


// note this are used by romtag
const char library_name[] = "musicport.audio";
const char id_string[] = "musicport ahi 2.2 (" __DATE__ ")\r\n";


struct ExecBase *SysBase;
struct Library * ExpansionBase;
struct DosLibrary *DOSBase;
struct Library *UtilityBase = NULL;

static BPTR _saved_seg_list;

static __saveds struct Library *_init_library(__reg("a6") struct ExecBase *sys_base, 
		__reg("a0") BPTR seg_list, 
		__reg("d0") struct Library *lib)
{
	SysBase = *(struct ExecBase **)4;
	_saved_seg_list = seg_list;

	lib->lib_Node.ln_Type = NT_LIBRARY;
	lib->lib_Node.ln_Name = (char *)library_name;
	lib->lib_Flags = LIBF_SUMUSED | LIBF_CHANGED;
	lib->lib_Version = 2;
	lib->lib_Revision = 2;
	lib->lib_IdString = (APTR)id_string;

	UtilityBase = OpenLibrary("utility.library", 37);
	if (UtilityBase != NULL)
	{
		DOSBase = (struct DosLibrary *)OpenLibrary("dos.library", 37);
		if (DOSBase != NULL)
		{
			// allright
			return lib;
		}
		else
		{
			CloseLibrary(UtilityBase);
		}
	}
	
	return NULL;
}

static __saveds BPTR _expunge(__reg("a6") struct Library *lib)
{
	if (lib->lib_OpenCnt != 0)
	{
		lib->lib_Flags |= LIBF_DELEXP;
		return 0;
	}

	if (DOSBase != NULL)
	{
#ifdef DEBUG	
		if (_conout != 0) 
		{
			Close(_conout);
			_conout = 0;
		}
#endif
		CloseLibrary((struct Library *)DOSBase);
		DOSBase = NULL;
	}
	if (UtilityBase != NULL)
	{
		CloseLibrary((struct Library *)UtilityBase);
		UtilityBase = NULL;
	}

	BPTR seg_list = _saved_seg_list;
	Remove(&lib->lib_Node);
	FreeMem((char *)lib - lib->lib_NegSize, lib->lib_NegSize + lib->lib_PosSize);
	return seg_list;
}

static void _open(__reg("a6") struct Library *lib)
{
	lib->lib_OpenCnt++;
}

static BPTR _close(__reg("a6") struct Library *lib)
{
	lib->lib_OpenCnt--;

	if (lib->lib_OpenCnt == 0 && (lib->lib_Flags & LIBF_DELEXP))
		return _expunge(lib);

	return 0;
}


// ahi driver begins here ------
static __saveds ULONG _child_proc_func();
static void _set_child_state(struct AHIAudioCtrlDrv *ac, mp_playstate_t ps);

static __saveds ULONG _alloc_audio(__reg("a1") struct TagItem *tags,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	_DEBUG("AllocAudio called!\n");		

	ULONG err = AHIE_UNKNOWN;
	if (ac->ahiac_DriverData == 0)
	{
		// allocate resources and initialize the audio hardware
		mp_ahi_t *dd = (mp_ahi_t *)AllocVec(sizeof(mp_ahi_t), MEMF_PUBLIC|MEMF_CLEAR);
		if (dd != NULL)
		{
			ac->ahiac_DriverData = dd;
			ac->ahiac_MixFreq = 48000;	// FIXME

			dd->ParentSigBit = AllocSignal(-1);
			if (dd->ParentSigBit != -1)
			{
				dd->ChildAttSigBit = -1;
				dd->Parent = FindTask(NULL);
				
				Forbid();
				dd->ChildProc = CreateNewProcTags(
					NP_Entry, _child_proc_func,
					NP_Name, "Musicport Task",
					NP_Priority, 25,
					TAG_DONE);
				if (dd->ChildProc != NULL)
				{
					dd->ChildProc->pr_Task.tc_UserData = ac;
				}
				Permit();

				if (dd->ChildProc != NULL)
				{
					_DEBUG("waiting child task...");
					Wait(1 << dd->ParentSigBit);
					_DEBUG("done!\n");
					
					// musicport can play any size but (currently) does no mixing
					ULONG flags = AHISF_KNOWSTEREO | AHISF_MIXING | AHISF_TIMING;
					_DEBUG("AllocAudio returned flags=%lx\n", flags);
					return flags;
				}
				else _DEBUG("error: cant create child task\n");
			}
			else _DEBUG("error: cant allocate parent signal\n");
			
			ac->ahiac_DriverData = NULL;
			FreeVec(dd);
		}
		else
		{
			err = AHIE_NOMEM;
			_DEBUG("error: no mem for driver data\n");
		}
	}
	else _DEBUG("error: already allocated!\n");
	
	return err;
}

static __saveds void _free_audio(__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	_DEBUG("FreeAudio called!\n");

	mp_ahi_t *dd = ac->ahiac_DriverData;
	if (dd != NULL)
	{
		if (dd->ChildProc != NULL)
		{
			Signal(&dd->ChildProc->pr_Task, 1 << dd->ChildAttSigBit);
			Wait(1 << dd->ParentSigBit);
			_DEBUG("child proc killed\n");
		}
		
		if (dd->ParentSigBit != -1)
			FreeSignal(dd->ParentSigBit);

		ac->ahiac_DriverData = NULL;
			
		FreeVec(dd);
		_DEBUG("driver data freed\n");
	}
}

static __saveds void _disable(__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	//TODO disable our actual interrupt instead
	Disable(); 
}

static __saveds void _enable(__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	//TODO enable our actual interrupt instead
	Enable(); 
}

const char *_sf[] = { "NONE", "PLAY", "REC", "PLAY+REC" };

static __saveds ULONG _start(__reg("d0") ULONG flags, 
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	_DEBUG("Start (%ld=%s)\n", flags, _sf[flags & 3]);

	ULONG err = AHIE_UNKNOWN;
	mp_ahi_t *dd = (mp_ahi_t *)ac->ahiac_DriverData;
	if (flags & AHISF_PLAY)
	{
		//TODO: stop if already playing
		
		dd->Config = (i2s_config_t) { .SampleRate = 48000 };	// FIXME
		mp_error_t mpe = musicport_init(&dd->Config);
		if (mpe == MPE_OK)
		{
			void *buf0 = AllocVec(ac->ahiac_BuffSize, MEMF_PUBLIC|MEMF_CLEAR);
			void *buf1 = AllocVec(ac->ahiac_BuffSize, MEMF_PUBLIC|MEMF_CLEAR);
			if (buf0 != NULL && buf1 != NULL)
			{
				i2s_double_buffer_t *dbuf = &dd->DBuf;
				dbuf->Buffer[0] = buf0;
				dbuf->Buffer[1] = buf1;
				dbuf->Length = 0;
				CacheClearE(buf0, ac->ahiac_BuffSize, CACRF_ClearD);
				CacheClearE(buf1, ac->ahiac_BuffSize, CACRF_ClearD);
				 
				_DEBUG("begin play (%ld bytes, ahiac_Flags=%ld)...\n", 
					ac->ahiac_BuffSize, ac->ahiac_Flags);
				_DEBUG("MinBuffSamples=%ld, MaxBuffSamples=%ld\n", 
					ac->ahiac_MinBuffSamples, ac->ahiac_MaxBuffSamples);
				_DEBUG("Channels=%ld, Sounds=%ld, BuffType=%lx\n", 
					ac->ahiac_Channels, ac->ahiac_Sounds, ac->ahiac_BuffType);
	
				_set_child_state(ac, MPPS_PLAY);
				return AHIE_OK;
			}
			else
			{
				_DEBUG("not enough memory for buffers\n");
				err = AHIE_NOMEM;
			}
			
			if (buf0 != NULL) FreeVec(buf0);
			if (buf1 != NULL) FreeVec(buf1);
		}
		else _DEBUG("musicport init failed (%ld)\n", mpe);
	}
	return err;
}

static __saveds ULONG _update(__reg("d0") ULONG flags,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	_DEBUG("Update (%ld=%s)\n", flags, _sf[flags & 3]);

	//TODO
	return AHIS_UNKNOWN;
}

static __saveds ULONG _stop(__reg("d0") ULONG flags,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	_DEBUG("Stop (%ld=%s)\n", flags, _sf[flags & 3]);
	
	mp_ahi_t *dd = (mp_ahi_t *)ac->ahiac_DriverData;
	if (flags & AHISF_PLAY)
	{
		if (dd->PlayState != MPPS_STOP)
		{
			_DEBUG("stop play...\n"); 
				
			_set_child_state(ac, MPPS_STOP);
			
			i2s_double_buffer_t *dbuf = &dd->DBuf;
			FreeVec((char *)dbuf->Buffer[0]);
			FreeVec((char *)dbuf->Buffer[1]);
		}
		else _DEBUG("already stopped\n");
	}

	return AHIS_UNKNOWN;
}

static __saveds ULONG _set_vol(__reg("d0") UWORD ch, 
	__reg("d1") Fixed vol, __reg("d2") sposition pan,
	__reg("a2") struct AHIAudioCtrlDrv *ac,
	__reg("d3") ULONG flags)
{
	//NOTE: its safe to call this function from interrupt
//	_DEBUG("SetVol called\n");
	//TODO
	return AHIS_UNKNOWN;
}

static __saveds ULONG _set_freq(__reg("d0") UWORD ch,
	__reg("d1") ULONG freq,
	__reg("a2") struct AHIAudioCtrlDrv *ac,
	__reg("d2") ULONG flags)
{
	//NOTE: its safe to call this function from interrupt
//	_DEBUG("SetFreq called\n");
	//TODO
	return AHIS_UNKNOWN;
}
	
static __saveds ULONG _set_sound(__reg("d0") UWORD ch,
	__reg("d1") UWORD sound, 
	__reg("d2") ULONG offset, __reg("d3") LONG offset,
	__reg("a2") struct AHIAudioCtrlDrv *ac,
	__reg("d4") ULONG flags)
{
	//NOTE: its safe to call this function from interrupt
//	_DEBUG("SetSound called\n");
	//TODO
	return AHIS_UNKNOWN;
}

static __saveds ULONG _set_effect(__reg("a0") APTR effect,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	//NOTE: this function is NOT to be called from interrupt
//	_DEBUG("SetEffect called\n");
	//TODO
	return AHIS_UNKNOWN;
}

static __saveds ULONG _load_sound(__reg("d0") UWORD sound, 
	__reg("d1") ULONG type, __reg("a0") APTR info,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	//NOTE: this function is NOT to be called from interrupt
#if 1
	static const char *_sitype[] = { "M8S", "M16S", "S8S", "S16S" };

	_DEBUG("LoadSound: sound=%lx type=%lx\n", (ULONG)sound, type);
	if (type == AHIST_SAMPLE || type == AHIST_DYNAMICSAMPLE)
	{
		struct AHISampleInfo *si = (struct AHISampleInfo *)info;
		_DEBUG("SampleInfo: type=%lx (%s) length=%lx\n", 
			si->ahisi_Type, (si->ahisi_Type < 4) ? _sitype[si->ahisi_Type] : "?",  
			si->ahisi_Length);
	}
#endif 			

	//TODO
	return AHIS_UNKNOWN;
}

static __saveds ULONG _unload_sound(__reg("d0") UWORD sound,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	//NOTE: this function is NOT to be called from interrupt
#if 0
	_DEBUG("UnloadSound: sound=%lx\n", (ULONG)sound);
#endif

	//TODO
	return AHIS_UNKNOWN;
}

static const ULONG _freq_table[] = { 48000 };
static const _freq_count = sizeof(_freq_table)/sizeof(ULONG);

static ULONG _find_freq(ULONG arg)
{
	for(unsigned i = 0; i < _freq_count; i++)
		if (_freq_table[i] == arg) return i;
	return 0;
}

static __saveds ULONG _get_attr(__reg("d0") ULONG attr,
	__reg("d1") LONG arg, __reg("d2") LONG defval,
	__reg("a1") struct TagItem *tags,
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	
	switch(attr)
	{
	case AHIDB_Bits:
		return 16;
	case AHIDB_Frequencies:
		return _freq_count;
	case AHIDB_Frequency:
		return (arg >= 0 && arg < _freq_count) ? _freq_table[arg] : 0;
	case AHIDB_Index:
		return _find_freq(arg);

	case AHIDB_Author:
		return (ULONG)"Miguel Fides";
	case AHIDB_Copyright:
#ifdef DEBUG
		return (ULONG)"build "__DATE__" "__TIME__;
#else
		return (ULONG)"free to use";
#endif
	case AHIDB_Version:
		return (ULONG)id_string;
	case AHIDB_Annotation:
		return (ULONG)"Musicport/Tsunami/Trinity audio support";
	
	case AHIDB_Record:
		return FALSE;
	case AHIDB_FullDuplex:
		return FALSE;
	case AHIDB_Realtime:
		return TRUE;
	
	case AHIDB_MaxPlaySamples:
		_DEBUG("MaxPlaySamples is %ld\n", defval);
		return defval;	// FIXME
	
	case AHIDB_MinMonitorVolume:
		return 0;
	case AHIDB_MaxMonitorVolume:
		return 0xFFFF;
	case AHIDB_MinOutputVolume:
		return 0;
	case AHIDB_MaxOutputVolume:
		return 0xFFFF;
	
	case AHIDB_Inputs:
		return 1;
	case AHIDB_Outputs:
		return 1;
	case AHIDB_Output:
		return (LONG)"Line";

	default:
		_DEBUG("GetAttr attr=%lx defval=%lx\n", attr, defval);
		return defval; 	
	}
}

static __saveds _hw_control(__reg("d0") ULONG attr,
	__reg("d1") LONG arg, 
	__reg("a2") struct AHIAudioCtrlDrv *ac)
{
	mp_ahi_t *dd = (mp_ahi_t *)ac->ahiac_DriverData;
	LONG rc = TRUE;
	mp_error_t err;

	switch(attr)
	{
		//TODO
	case AHIC_Output:
		_DEBUG("HWCtrl: set output=%lx\n", arg);
		break;
	case AHIC_Output_Query:
		_DEBUG("HWCtrl: get output\n");
		rc = 0;
		break;
	case AHIC_MonitorVolume:
		dd->MonitorVol = arg;
		_DEBUG("HWCtrl: MonitorVolume set (%lx)\n", (unsigned)dd->MonitorVol);

		err = musicport_set_mixer(MPMT_MONITOR, 0, (unsigned short)(dd->MonitorVol >> 16));
		if (err != MPE_OK)
			_DEBUG("set_mixer returned error %lx!\n", err);
		break;
	case AHIC_MonitorVolume_Query:
		// NOTE: vol is returned in rc, arg (pointer) is ignored
		rc = (ULONG)dd->MonitorVol;
		_DEBUG("HWCtrl: MonitorVolume_Query returned %lx\n", rc);
		break;
		
	default:
		_DEBUG("HWCtrl: attr=%lx\n", attr);
		rc = FALSE;
		break; 	
	}
	return rc;
}

static void _set_child_state(struct AHIAudioCtrlDrv *ac, mp_playstate_t ps)
{
	mp_ahi_t *dd = (mp_ahi_t *)ac->ahiac_DriverData;
	if (dd->PlayState != ps)
	{
		dd->PlayState = ps;
		Signal(&dd->ChildProc->pr_Task, 1 << dd->ChildPlaySigBit);
		_DEBUG("playstate changed (%ld)\n", (ULONG)ps);
	}
	else _DEBUG("state change is already done\n");
}


static __saveds ULONG _child_proc_func()
{
	struct Process *me = (struct Process *)FindTask(NULL);
	struct AHIAudioCtrlDrv *ac = (struct AHIAudioCtrlDrv *)me->pr_Task.tc_UserData;

	ULONG interrupt_sig_mask = 0;

	mp_ahi_t *dd = (ac != NULL) ? ac->ahiac_DriverData : NULL;	
	if (dd != NULL)
	{
		dd->ChildAttSigBit = AllocSignal(-1);
		dd->ChildPlaySigBit = AllocSignal(-1);

		_DEBUG("child: running\n");
		Signal(dd->Parent, 1 << dd->ParentSigBit);

		while(1)
		{
			ULONG mask = Wait((1 << dd->ChildAttSigBit)
				| (1 << dd->ChildPlaySigBit) 
				| interrupt_sig_mask);
			
			if (mask == interrupt_sig_mask)
			{
				unsigned index = dd->DBuf.ActiveIndex & 1;
				void *ptr = dd->DBuf.Buffer[index];
				
				CallHookPkt(ac->ahiac_PlayerFunc, ac, NULL);
				if (!( ((mp_ahi_fptr_t)ac->ahiac_PreTimer)(ac) ))
				{
					CallHookPkt(ac->ahiac_MixerFunc, ac, ptr);
					CacheClearE(ptr, ac->ahiac_BuffSize, CACRF_ClearD);
				}
				((mp_ahi_fptr_t)ac->ahiac_PostTimer)(ac);
				
				if (ac->ahiac_BuffSamples != dd->DBuf.Length)
				{
					_DEBUG("%ld ", ac->ahiac_BuffSamples);
					dd->DBuf.Length = ac->ahiac_BuffSamples;
				}
			}
			else if (mask == (1 << dd->ChildPlaySigBit))
			{
				switch(dd->PlayState)
				{
				case MPPS_PLAY:
					if (dd->DBuf.Length == 0)
					{
						interrupt_sig_mask = musicport_play(&dd->DBuf, 
							ac->ahiac_MaxBuffSamples);
						_DEBUG("child: start play (%ld)...\n", 
							dd->DBuf.Length);
					}
					break;
				case MPPS_STOP:
					if (dd->DBuf.Length != 0)
					{
						_DEBUG("child: stop play...\n");
						musicport_stop();
						interrupt_sig_mask = 0;
						dd->DBuf.Length = 0;
					}
					break;
				}
			}
			else if (mask == (1 << dd->ChildAttSigBit))
			{
				break;
			}
		}
		
		_DEBUG("child: stopping\n");
		Signal(dd->Parent, 1 << dd->ParentSigBit);
	}
	else _DEBUG("child: error (dd is NULL)\n");

	return 0;
}


static ULONG _library_vectors[] =
{
	(ULONG)_open,
	(ULONG)_close,
	(ULONG)_expunge,
	0,
	(ULONG)_alloc_audio,	// a1,a2
	(ULONG)_free_audio,		// a2
	(ULONG)_disable,		// a2
	(ULONG)_enable,			// a2
	(ULONG)_start,			// d0,a2
	(ULONG)_update,			// d0,a2
	(ULONG)_stop,			// d0,a2
	(ULONG)_set_vol,		// d0,d1,d2,a2,d3
	(ULONG)_set_freq,		// d0,d1,a2,d2
	(ULONG)_set_sound,		// d0,d1,d2,d3,a2,d4
	(ULONG)_set_effect,		// a0,a2
	(ULONG)_load_sound,		// d0,d1,a0,a2
	(ULONG)_unload_sound,	// d0,a2
	(ULONG)_get_attr,		// d0,d1,d2,a1,a2
	(ULONG)_hw_control,		// d0,d1,a2
	-1,
};

ULONG auto_init_tables[] =
{
	sizeof(struct Library),	/* dataSize */
	(ULONG)_library_vectors,	/* vectors */
	0,						/* structure (for InitStruct()) */
	(ULONG)_init_library,	/* init function */
};

