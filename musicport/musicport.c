#include "musicport.h"
#include <support/i2c_hal.h>
#include "wm8731.h"

static bool _init_i2c()
{
	return hal_i2c_initialize(0, 200000UL);
}

mp_error_t musicport_init(const i2s_config_t *cfg)
{
	if (cfg == NULL)
		return MPE_UNKNOWN;

	mp_error_t err = MPE_UNKNOWN;
	if (_init_i2c())
	{
		if (wm8731_init(cfg))
		{
			if (hal_i2s_init_slave(cfg))
			{
				return MPE_OK;
			} else err = MPE_I2S_ERROR;
		} // FIXME: error is probably unsupported freq by codec 
	} else err = MPE_I2C_ERROR;
	return err;
}

mp_error_t musicport_set_mixer(mp_mixer_type_t mt, unsigned short index, unsigned short value)
{
	mp_error_t err = MPE_UNKNOWN;

	switch(mt)
	{
		case MPMT_MONITOR:
			if (_init_i2c())
			{
				wm8731_set_monitor((char)(value >> 9));
				err = MPE_OK;
			}
			break;
	}
	return err;
}


ULONG musicport_play(i2s_double_buffer_t *buf, unsigned short length)
{
	buf->Length = length;
	ULONG sig_mask = hal_i2s_start(buf);
	return sig_mask;
}

void musicport_stop()
{
	hal_i2s_stop();
}
