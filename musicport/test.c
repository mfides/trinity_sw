#include <exec/exec.h>

#pragma dontwarn 61
#include <devices/ahi.h>
#include <libraries/ahi_sub.h>
#pragma popwarn

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/ahi.h>

#include <stdio.h>
#include <stdbool.h>

struct Library *AHIBase = NULL;

static bool _str2uint(const char *s, unsigned int *pvalue);

typedef struct
{
	struct MsgPort *ac_MsgPort;
	struct AHIRequest *ac_IO;
} ahi_context_t;

static bool _open_ahi(ahi_context_t *ahi);
static void _close_ahi(ahi_context_t *ahi);

int main(int argc, const char *argv[])
{
	LONG  arg_array[] = { 0, 0 };
	static const char *template = "MODE/A,VOL/N";

	struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
				
	if (rdargs != NULL)
	{
		const char *mode = (const char *)arg_array[0];
		unsigned int mode_id;
		if (_str2uint(mode, &mode_id))
		{
			printf("mode id:  %x\n", mode_id);
			
			ahi_context_t ahi;
			if (_open_ahi(&ahi))
			{
				struct AHIAudioCtrl *actrl = AHI_AllocAudio(
					AHIA_AudioID, mode_id,
					AHIA_Channels, 1,
					AHIA_Sounds, 2,
					TAG_DONE);
				if (actrl != NULL)
	{
					ULONG vol = 0xFFFF << 16;
					if (arg_array[1] != 0)
					{
						ULONG value = *(ULONG *) arg_array[1];
						vol = ((0xFFFF * value) / 100) << 16;

						printf("setting monitor volume %lx...\n", vol);
	}

					Fixed old = 0;	// 16 bit fixed-point decimal
					ULONG error = AHI_ControlAudio(actrl,
						AHIC_MonitorVolume_Query, (ULONG)&old,
						AHIC_MonitorVolume, vol,
						TAG_DONE);
					
					if (error == 0)
	{
						printf("previous monitor volume was %x\n", (unsigned)old);
					}
					else printf("AHI_ControlAudio returned error %u!", (unsigned)error);
					
					AHI_FreeAudio(actrl);
				}
				else printf("can't alloc audio control\n");
						
				_close_ahi(&ahi);
			}
			else printf("can't open ahi device!\n");
		}
		else printf("mode '%s' is not a unsigned number\n", mode);
	}
	else printf("usage: %s\n", template);

//	AHIsubBase = OpenLibrary("musicport.audio", 2L);
//	if (AHIsubBase != NULL)
//	{
//		printf("musicport.audio ready\n");
//		CloseLibrary(AHIsubBase);
//	}
//	else printf("Could not open musicport.audio\n");

	return 0;
}

static bool _open_ahi(ahi_context_t *ahi)
{
	struct MsgPort *mp = CreateMsgPort();
	if (mp != NULL)
	{
		struct IORequest *io = CreateIORequest(mp, sizeof(struct AHIRequest));
		if (io != NULL)
		{
			ahi->ac_MsgPort = mp;
			ahi->ac_IO = (struct AHIRequest *)io;
			ahi->ac_IO->ahir_Version = 4;

			BYTE error = OpenDevice(AHINAME, AHI_NO_UNIT, io, 0UL);
			if (error == 0)
			{
				AHIBase = (struct Library *)io->io_Device;
				return true; 
			}
			else printf("OpenDevice returned error %u\n", (unsigned)error);
			
			DeleteIORequest(io);
		}
		else printf("CreateIORequest returned NULL\n");
		
		DeleteMsgPort(mp);	
	}
	else printf("CreateMsgPort return NULL\n");
	
	return false;
}

static void _close_ahi(ahi_context_t *ahi)
{
	CloseDevice((struct IORequest *)ahi->ac_IO);
	DeleteIORequest((struct IORequest *)ahi->ac_IO);
	DeleteMsgPort(ahi->ac_MsgPort);
}




static bool _hex2uint(const char *s, unsigned int *pvalue)
{
	bool done = false;
	unsigned int value = 0;
	char a;
	while(a = *s++, a != '\0')
	{
		unsigned value2 = value << 4;
		if (a >= '0' && a <= '9')
		{
			value2 += (unsigned)(a - '0');
		}
		else if (a >= 'a' && a <= 'f')
		{
			value2 += 10 + (unsigned)(a - 'a');
		}
		else if (a >= 'A' && a <= 'F')
		{
			value2 += 10 + (unsigned)(a - 'A');
		}
		else if (a == '\0' || a == ' ')
		{
			break;
		}
		else 
		{
			done = false;
			break;
		}
		
		if (value2 < value) 
		{
			done = false;
			break;
		}
		else
		{
			done = true;
			value = value2;
		}
	}
	if (done) *pvalue = value;
	return done;
}

static bool _str2uint(const char *s, unsigned int *pvalue)
{
	while(*s == ' ') s++;
	if (*s == '$')
	{
		s++;
		return _hex2uint(s, pvalue); 
	}
	
	bool done = false;
	unsigned int value = 0;
	char a;
	while(a = *s++, a != '\0')
	{
		if (a >= '0' && a <= '9')
		{
			unsigned int value2 = (value * 10) + (unsigned)(a - '0');
			if (value2 < value) 
			{
				done = false;
				break;
			}
			done = true;
			value = value2;
		}
		else if (a == '\0' || a == ' ')
		{
			break;
		}
		else 
		{
			done = false;
			break;
		}
	}
	if (done) *pvalue = value;
	return done;
}

