
	incdir	include_i:
	include	devices/ahi.i
	include	libraries/ahi_sub.i

TRUE		EQU	1
FALSE		EQU	0

BEG:

*** FORM AHIM
	dc.l	ID_FORM
	dc.l	E-S
S:
	dc.l	ID_AHIM


*** AUDN
	dc.l	ID_AUDN
	dc.l	.e-.s
.s:
	dc.b	"musicport",0
.e:
	CNOP	0,2

*** AUDM
ModeA:
	dc.l	ID_AUDM
	dc.l	.e-.s
.s
	dc.l	AHIDB_AudioID,		$00AE0001

	dc.l	AHIDB_Volume,		TRUE
	dc.l	AHIDB_Panning,		TRUE
	dc.l	AHIDB_Stereo,		TRUE
	dc.l	AHIDB_HiFi,		FALSE
	dc.l	AHIDB_MultTable,	FALSE

	dc.l	AHIDB_Name,		.name-.s
	dc.l	TAG_DONE
.name	dc.b	"Musicport:16 bit stereo",0
.e
	CNOP	0,2

E:
	CNOP	0,2
END:
