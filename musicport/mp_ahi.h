#ifndef MUSICPORT_AHI_H
#define MUSICPORT_AHI_H

#include <dos/dos.h>
#include "musicport.h"
 
typedef enum { MPPS_STOP = 0, MPPS_PLAY = 1 } mp_playstate_t;

typedef struct
{
	i2s_config_t Config;
	i2s_double_buffer_t DBuf;
    struct Task *Parent;
    struct Process *ChildProc;
    mp_playstate_t PlayState; 
    BYTE ParentSigBit;
    BYTE ChildAttSigBit;
    BYTE ChildPlaySigBit;
    Fixed MonitorVol;
} mp_ahi_t;

typedef ULONG (*mp_ahi_fptr_t)(__reg("a2") struct AHIAudioCtrlDrv *);

#endif // MUSICPORT_AHI_H

