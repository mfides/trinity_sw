#ifndef MUSICPORT_H
#define MUSICPORT_H

#include <exec/types.h>
#include <support/i2s_hal.h>

typedef enum
{
	MPE_OK = 0,
	MPE_UNKNOWN,
	MPE_I2C_ERROR,
	MPE_I2S_ERROR,
} mp_error_t;

typedef enum
{
	MPMT_PLAY = 0,
	MPMT_RECORD,
	MPMT_MONITOR,
} mp_mixer_type_t;

mp_error_t musicport_init(const i2s_config_t *cfg);
ULONG musicport_play(i2s_double_buffer_t *buf, unsigned short length);
void musicport_stop();

mp_error_t musicport_set_mixer(mp_mixer_type_t mt, unsigned short index, unsigned short value); 

#endif // MUSICPORT_H




