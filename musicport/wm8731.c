#include "wm8731.h"
#include <support/i2c_hal.h>
//#include <assert.h>

#ifndef WM8731_I2C_ADDR
#define WM8731_I2C_ADDR 0x1a // use 0x1b for SDB tied high
#endif 

#ifndef WM8731_I2C_MODULE
#define WM8731_I2C_MODULE 0
#endif

static bool _bypass = false;

static void _init_power(bool force);
static bool _update_analog_path();

bool wm8731_init(const i2s_config_t *cfg)
{
	_init_power(true);

	wm8731_write(WM8731_REG_IF_FORMAT,	// master mode 
		WM8731_IFF_FORMAT_I2S | WM8731_IFF_IWL_16BIT | WM8731_IFF_MS);
	switch(cfg->SampleRate)
	{
		case 32000:		// BOSR = 0 (256fs); 32KHz @ 12.288MHz MCLK
			wm8731_write(WM8731_REG_SAMPLING, 6 /*0b0110*/ << WM8731_SMPL_SR_BIT);	// normal mode;
			wm8731_write(WM8731_REG_DIGITAL_PATH, WM8731_DP_DEEMPH_32KHZ | WM8731_DP_ADCHPD);
			break;
		case 48000:		// BOSR = 0 (256fs); 48KHz @ 12.288MHz MCLK
			wm8731_write(WM8731_REG_SAMPLING, 0 /*0b0000*/ << WM8731_SMPL_SR_BIT);	// normal mode;
			wm8731_write(WM8731_REG_DIGITAL_PATH, WM8731_DP_DEEMPH_48KHZ | WM8731_DP_ADCHPD);
			break;
		default:
//			panic(KERNEL_ERROR_NOT_SUPPORTED);
			return false;
	}

	wm8731_write(WM8731_REG_HP_LEFT, WM8731_HP_BOTH | 127);	// max 127
	_update_analog_path();

	wm8731_write(WM8731_REG_ACTIVE, 1);
	return true;
}

static void _init_power(bool force)
{
	static bool _init_done = false;
	
	if (!_init_done || force)
	{
		wm8731_write(WM8731_REG_POWER_DOWN, WM8731_PD_MICPD | WM8731_PD_ADCPC);
		_init_done = true;
	}
}

static bool _update_analog_path()
{
	unsigned ap = WM8731_AP_DACSEL;
	if (_bypass) ap |= WM8731_AP_BYPASS;
	return wm8731_write(WM8731_REG_ANALOG_PATH, ap);
}

bool wm8731_set_monitor(signed char value)
{
	const unsigned char vol0db = 0x17;	// max volume for no amplification

	_init_power(false);
	
	if (value <= 0)
	{
		_bypass = false;
	}
	else
	{
		wm8731_write(WM8731_REG_POWER_DOWN, WM8731_PD_MICPD | WM8731_PD_ADCPC);
		unsigned vol5 = ((vol0db * (unsigned)value) + 0x7f) >> 7;
		wm8731_write(WM8731_REG_LINE_LEFT, WM8731_LINE_BOTH | vol5);
		_bypass = true;
	}
	return _update_analog_path();
}

bool wm8731_write(wm8731_reg_t reg, unsigned value)
{
	unsigned char data[] = { (reg << 1) | ((value >> 8) & 1), value };
	i2c_context_t i2c;
	hal_i2c_init_context(&i2c, WM8731_I2C_ADDR, data, sizeof(data));
	bool done = hal_i2c_write(WM8731_I2C_MODULE, &i2c, NULL, 0);
//	assert(done);
	return done;
}

