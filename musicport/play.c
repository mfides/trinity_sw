#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "hxcmod.h"
#include "musicport.h"

#include <dos/dos.h>
#include <exec/exec.h>

#include <proto/exec.h>
#include <proto/dos.h>

struct Library * ExpansionBase;

#define AU_BUF_LENGTH 4096 //24000
static ULONG _buf0[AU_BUF_LENGTH];
static ULONG _buf1[AU_BUF_LENGTH];
static i2s_double_buffer_t _dbuf;
static modcontext _mod_context;
static tracker_buffer_state _tracker;

#define MP_SAMPLE_RATE 48000 

int main(int argc, char *argv[])
{
	int done = hxcmod_init(&_mod_context);
	assert(done);

	unsigned mod_length = 0;
	void *mod_ptr = NULL;
	if (argc >= 2)
	{
		struct FileInfoBlock info;
		const char *file_name = argv[1];
		
		mod_length = 0;
		BPTR lock = Lock(file_name, ACCESS_READ);
		if (lock)
		{
			if (Examine(lock, &info))
			{
				mod_length = info.fib_Size; 
			}
			UnLock(lock);
		}
		
		if (mod_length != 0)
		{
			printf("file size: %u\n", mod_length);
			mod_ptr = malloc(mod_length);
			if (mod_ptr)
			{
				BPTR fh = Open(file_name, MODE_OLDFILE);
				if (fh)
				{
					LONG read = Read(fh, mod_ptr, mod_length);
					if (read == mod_length)
					{
						printf("mod loaded at $%lx\n", (ULONG)mod_ptr);
					} 
					else 
					{
						printf("mod truncated!\n");
						mod_length = 0;
					}
				
					Close(fh);
				}
				else
				{
					printf("could not open file\n");
					mod_length = 0;
				}
			} 
			else 
			{
				printf("not enough memory!\n");
				mod_length = 0;
			}
		} else printf("file '%s' not found\n", file_name);
	} 
	fflush(stdout);

	if (mod_ptr == NULL)
	{
		return -1;
	} 
	else if (mod_length == 0)
	{
		free(mod_ptr);
		return -2;
	}
	else 
	{
		int error = hxcmod_load(&_mod_context, mod_ptr, mod_length);
		if (error != 0)
		{
			printf("mod not recognized (error %d)\n", error);
			
			free(mod_ptr);
			return -3;
		}
	} 

	hxcmod_setcfg(&_mod_context, MP_SAMPLE_RATE, 1, 0);

	printf("Musicport/amigaos test\n");

	i2s_config_t i2s_cfg = (i2s_config_t) { .SampleRate = MP_SAMPLE_RATE };
	mp_error_t mpe = musicport_init(&i2s_cfg);
	if (mpe != MPE_OK)
	{
		printf("Init failed (error %d)!\n", (int)mpe);
		return 1;
	}
	
	_dbuf.Buffer[0] = _buf0;
	_dbuf.Buffer[1] = _buf1;
//	hxcmod_fillbuffer(&_mod_context, (void *)_buf0, AU_BUF_LENGTH, &_tracker);
	ULONG sig_mask = musicport_play(&_dbuf, AU_BUF_LENGTH);

	printf("(stop with CTRL-C) waiting...");
	fflush(stdout);
	
	unsigned wait_mask = SIGBREAKF_CTRL_C | sig_mask;
	while(1)
	{
		ULONG done_mask = Wait(wait_mask);
		if (done_mask == SIGBREAKF_CTRL_C)
		{
			printf("break!\n");
			break;
		}
		else 
		{
			unsigned index = _dbuf.ActiveIndex & 1;
			void *ptr = _dbuf.Buffer[index];
			
			hxcmod_fillbuffer(&_mod_context, ptr, AU_BUF_LENGTH, &_tracker);
			CacheClearE(ptr, AU_BUF_LENGTH << 2, CACRF_ClearD);

//			printf("%s", index ? "1" : "0");
//			fflush(stdout);
		}
	}

	free(mod_ptr);
	musicport_stop();
}

void _chkabort()
{
}





