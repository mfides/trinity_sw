#ifndef TRINITY_DMA_H
#define TRINITY_DMA_H

#include "trinity.h"
#include <exec/interrupts.h>

typedef struct tnt_dma tnt_dma_t;

#define DMACON_DONE 		(1UL<<0)
#define DMACON_START 		(1UL<<1)
#define DMACON_PSEL_MASK 	(3UL<<4)
#define DMACON_PSEL_MEM 	(0UL<<4)
#define DMACON_PSEL_DAC 	(1UL<<4)
#define DMACON_INTEN 		(1UL<<6)
#define DMACON_LOOP			(1UL<<7)


typedef void (*dma_handler_t)(unsigned module);

typedef struct 
{
	unsigned Module;
	dma_handler_t Handler;
	int SigBit;
	struct Task *ServerTask;
	struct Interrupt Interrupt;
} dma_interrupt_t;


void dma_start(unsigned module, void *src, unsigned short length, ULONG control);
void dma_reload(unsigned module, void *src, unsigned short length);
void dma_stop(unsigned module);
ULONG dma_add_int_handler(unsigned module, dma_interrupt_t *di, dma_handler_t handler);
void dma_rem_int_handler(unsigned module);


#endif // TRINITY_DMA_H



