#include "trinity.h"
//#include <stdio.h>

#include <exec/types.h>
#include <exec/interrupts.h>
#include <hardware/intbits.h>

#include <proto/exec.h>
#include <proto/expansion.h>
//#include <proto/dos.h>

#define AUTOCONFIG_MANUF 5110
#define AUTOCONFIG_PROD 61

tnt_control_t *trinity_find()
{
	if (ExpansionBase == NULL)
		ExpansionBase = (struct ExpansionBase *)OpenLibrary("expansion.library", 0L);
	
		
	if (ExpansionBase != NULL)
	{	
		struct ConfigDev *cd = NULL;
		cd = FindConfigDev(cd, AUTOCONFIG_MANUF, AUTOCONFIG_PROD);
		if (cd != NULL) 
		{
#ifdef DEBUG		
//			printf("device located at $%lx\n", (ULONG)cd->cd_BoardAddr);
#endif	
			return (tnt_control_t *)cd->cd_BoardAddr;
		}
	}
	return NULL;
}


