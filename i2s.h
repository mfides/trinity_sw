#ifndef TRINITY_I2S_H
#define TRINITY_I2S_H

#include "trinity.h"

typedef struct tnt_i2s tnt_i2s_t;

#define I2SCON_EN_DAC	(1UL<<0)
#define I2SSTA_DMAREQ_DAC	(1UL<<4)

#define I2S_STATE_STOP 0
#define I2S_STATE_STOPPING 1
#define I2S_STATE_RUNNING 2


#endif // TRINITY_I2S_H
