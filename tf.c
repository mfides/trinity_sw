#include <stdio.h>
#include <stdbool.h>

#include <exec/types.h>
#include <exec/memory.h>
#include <support/spi_hal.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include "flash.h"

struct Library *ExpansionBase;
struct Library *UtilityBase;

static bool _read_info(flash_info_t *fi);
static void _view_block(const flash_info_t *fi, unsigned offset);
static bool _erase_all(const flash_info_t *fi);
static void _write_file(const flash_info_t *fi, const char *filename, unsigned block);
static void _print(const char *s);

int main(int argc, char *argv[])
{
	LONG  arg_array[] = { 0, 0, 0, 0 };
	static const char *template = "VIEW/N/K,ERASE/S,WRITE/K,OFFSET/N/K";

	printf("Trinity flash tool\n");
 	
	UtilityBase = (struct Library *)OpenLibrary("utility.library", 0L);
	if (UtilityBase != NULL)
	{
		flash_info_t fi;
		bool done = _read_info(&fi);
		if (done)
		{
#ifdef DEBUG
			printf("Flash manuf %02xh, Device %02x%02xh (%u more bytes)\n",
				fi.ManufacturerId, fi.DeviceId[0], fi.DeviceId[1],
				fi.ExtBytes);
#else
			printf("Flash manuf %02xh, Device %02x%02xh\n",
				fi.ManufacturerId, fi.DeviceId[0], fi.DeviceId[1]);
#endif
		}
		else printf("couldn't read flash info\n");

		if (done && argc > 0)
		{
			struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
				
			if (rdargs != NULL)
			{
				if (arg_array[0] != 0) // VIEW
				{
					unsigned offset = *(unsigned *)arg_array[0];
					printf("View at %u:\n", offset);
					_view_block(&fi, offset);
				}
				else if (arg_array[1] != 0) // ERASE
				{
					_print("Erase all...");
					_erase_all(&fi);
					printf("done!\n");
				}
				else if (arg_array[2] != 0) // WRITE
				{
					char *filename = (char *)arg_array[2];
					if (*filename != '\0')
					{
#ifdef DEBUG
						printf("Write binary file '%s'\n", filename);
#endif
						unsigned block = 0;
						if (arg_array[3] != 0) // OFFSET
						{
							block = *(unsigned *)arg_array[3];
#ifdef DEBUG
							printf("At block %u...\n", block);
#endif
						}

						_write_file(&fi, filename, block);
					}
				}
				
				FreeArgs(rdargs);
			} else printf("Use %s\n", template);
		}
		
		CloseLibrary((struct Library *)UtilityBase);
	} else printf("couldn't open utility.library\n");
	
	return 0;
}

static void _print(const char *s)
{
	printf(s);
	fflush(stdout);
}

static void _view_block(const flash_info_t *fi, unsigned offset)
{
	if (hal_spi_initialize(0, 100000UL))
	{
		static unsigned char cmd[4+256];
		cmd[0] = 0x03; // Read
		cmd[1] = offset>>16;
		cmd[2] = offset>>8;
		cmd[3] = offset;

		bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
		if (done)
		{
			unsigned char *data = &cmd[4];
			for(unsigned i = 0; i < 256;)
			{
				printf("%02x ", data[i++]);
				if (0 == (i & 15)) printf("\n");
			}
		} else printf("spi transmit error!\n");
	} else printf("spi init failed\n");	
}

static bool _read_status(const flash_info_t *fi, unsigned char *psta)
{
	unsigned char cmd[] = { 0x05, 0 };	// RDSR
	bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
	if (done)
	{
		*psta = cmd[1];
		return true;
	}
	return false;
}

static bool _write_enable(const flash_info_t *fi)
{
	unsigned char cmd[] = { 0x06 };	// WREN
	bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
	if (done)
	{
		unsigned char sta; 
		done = _read_status(fi, &sta);
		if (done)
		{
			if ((sta & 0x02) == 0x02) // check WEL bit
			{
				return true;
			}
			printf("error: write not enabled (sta=%02x)\n", sta);
		}
	}
	return false;
}

static bool _erase_all(const flash_info_t *fi)
{
	if (hal_spi_initialize(0, 100000UL))
	{
		if (_write_enable(fi))
		{
			unsigned char cmd[] = { 0x60 };	// Bulk Erase
			bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
			unsigned char sta; 
			while(done)
			{
				Delay(10);
				done = _read_status(fi, &sta);
				if (!done || !(sta & 0x01)) break;
				_print(".");
				Delay(20);
			}
			return done;
		}
		else printf("write enable failed\n");
	} else printf("spi init failed\n");
	return false;
}

static unsigned _erase_sector(const flash_info_t *fi, unsigned offset)
{
	static const unsigned _sector_size = 65536;	// FIXME: other sector sizes?
	
	if (_write_enable(fi))
	{
		unsigned char cmd[] = { 0xD8, 0, 0, 0 };	// Sector Erase
		cmd[1] = offset>>16;
		cmd[2] = offset>>8;
		cmd[3] = offset;
	
		bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
		unsigned char sta; 
		while(done)
		{
			Delay(10);
			done = _read_status(fi, &sta);
			if (!done || !(sta & 0x01)) break;
	#ifdef DEBUG
			_print(".");
	#endif
			Delay(30);
		}
	
		unsigned clear = _sector_size - (offset & (_sector_size - 1));
		return done ? clear : 0;
	}
	else printf("write enable failed\n");
	return 0;
}

static unsigned _page_program(const flash_info_t *fi, unsigned offset, unsigned char *data, unsigned size)
{
	if (size > 256) size = 256;	// FIXME: other page sizes?
	
	if (_write_enable(fi))
	{
		static unsigned char cmd[4+256];
		cmd[0] = 0x02; // Page Program
		cmd[1] = (unsigned char)(offset >> 16);
		cmd[2] = (unsigned char)(offset >> 8);
		cmd[3] = (unsigned char)offset;
	
		unsigned char *buf = &cmd[4];
		for(unsigned i = 0; i < size; i++) buf[i] = data[i];
	
		bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
		if (done)
		{
			unsigned wait = 0;
			unsigned char sta; 
			while(true)
			{
				done = _read_status(fi, &sta);
				if (!done || !(sta & 0x01)) break;
#ifdef DEBUG
				wait++;
				if (0 == (wait & 127)) _print(".");
#endif
			}
			if (done && (sta & 0xfc))
			{
				printf("error: page program (sta = %02xh)\n", sta);
				return 0;
			}
			return done ? size : 0;
		} else printf("spi transmit error!\n");
	}
	else printf("write enable failed\n");
	return 0;
}

static bool _flash_image(const flash_info_t *fi, unsigned char *image, unsigned size, unsigned offset)
{
	if (hal_spi_initialize(0, 1000000UL))
	{
		unsigned done = 0;
		while(done < size)
		{
			_print("#");
			int clear = (signed int)_erase_sector(fi, offset);
			if (clear > 0)
			{
				do
				{
#ifdef DEBUG
					_print("P");
#endif
					unsigned page = _page_program(fi, offset, image + done, clear);
					if (page != 0)
					{
						offset += page;
						done += page;
						clear -= page;
					}
					else 
					{
						printf("page program failed\n");
						break;
					}
				} while(clear > 0 && done < size);
			}
			else 
			{
				printf("error: sector is not clear\n");
				break;
			}
		}
		printf("\n");
		
		if (done >= size)
		{
			printf("Written %u bytes ok\n", done);
			return true;
		}
		else printf("error: write incomplete\n");
	} 
	else printf("spi init failed\n");
	
	return false;
}

static bool _check_image(unsigned char *image, unsigned size)
{
	if (size < 320000) return 0;
	
	unsigned offset = 0;
	while (offset < size && image[offset] == 0xFF) 
	{
		offset++;
		if (offset >= 256) return false;
	} 
	if (image[offset++] != 0xAA) return false;
	if (image[offset++] != 0x99) return false;
	if (image[offset++] != 0x55) return false;
	if (image[offset++] != 0x66) return false;
	return true;
}

static void _write_file(const flash_info_t *fi, const char *filename, unsigned block)
{
	BPTR lock = Lock(filename, ACCESS_READ);
	if (lock != 0)
	{
		struct FileInfoBlock fib;
		if (Examine(lock, &fib))
		{
			unsigned filesize = fib.fib_Size;
			printf("File length: %u\n", filesize);
			if (filesize != 0)
			{
				void *buffer = AllocVec(filesize, MEMF_CLEAR);
				if (buffer != NULL)
				{
					BPTR file = OpenFromLock(lock);
					if (file != 0)
					{
						// read entire file to memory
						int done = Read(file, buffer, filesize);
						if (done == filesize)
						{
#ifdef DEBUG
							printf("Read %d bytes from file\n", done);
#endif
							bool image_ok = true;
							if (block < 360) 
							{
								image_ok = _check_image(buffer, filesize);
							}
							if (image_ok)
							{
								printf("File image checks ok\n");
								_flash_image(fi, buffer, filesize, block * 1024UL);
							}
							else printf("file image checks failed\n");
						}
						else printf("couldn't read file!\n");
					
						Close(file);
					}
					else printf("couldn't open file!\n");
					
					FreeVec(buffer);
				}
				else printf("not enough memory to load file\n");
			}
			else printf("file is empty!\n");
		}
		else printf("file info read error\n");
		
		UnLock(lock);
	}
	else printf("error locking file!\n");
}

static bool _read_info(flash_info_t *fi)
{
	if (hal_spi_initialize(0, 100000UL))
	{
		unsigned char cmd[] = { 0x9F, 0, 0, 0, 0 };	// RDID
		bool done = hal_spi_transmit(0, cmd, cmd, sizeof(cmd));
		if (done)
		{
			*fi = *(flash_info_t *)&cmd[1];
			return true;
		} else printf("spi transmit error!\n");
	} else printf("spi init failed\n");
	return false;
}


