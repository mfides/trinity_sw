#ifndef SUPPORT_SPI_HAL_H
#define SUPPORT_SPI_HAL_H

#include <stddef.h>
#include <stdbool.h>

// prototypes
bool hal_spi_initialize(unsigned module, unsigned bitrate);
bool hal_spi_transmit(unsigned module, const unsigned char *outbuf, unsigned char *inbuf, unsigned length);

#endif // SUPPORT_SPI_HAL_H

