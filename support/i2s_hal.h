#ifndef I2S_HAL_H
#define I2S_HAL_H

#include <exec/types.h>
#include <stdbool.h>

typedef struct
{
	unsigned SampleRate;
	unsigned char SampleBits;
	unsigned char FormatFlags;
} i2s_config_t;

typedef enum
{
	I2S_FF_NONE = 0,	// default is left-aligned 
	I2S_FF_MONO = 1<<0,
	I2S_FF_I2S = 1<<1,	// skips first bit (MSB is second bit)
} i2s_format_flags_t;

typedef struct
{
	void *Buffer[2];
	unsigned char ActiveIndex;
	unsigned short Length;
} i2s_double_buffer_t;

bool hal_i2s_init_slave(const i2s_config_t *cfg);
ULONG hal_i2s_start(i2s_double_buffer_t *buf);
void hal_i2s_stop();

#endif // I2S_HAL_H


