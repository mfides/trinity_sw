#ifndef SUPPORT_I2C_HAL_H
#define SUPPORT_I2C_HAL_H

//#include <kernel/event.h>
//#include <kernel/panic.h>
#include <stddef.h>
#include <stdbool.h>
//#include <assert.h>

typedef enum
{
	I2C_OK = 0,
	I2C_NACK,
	I2C_NO_RESPONSE,
	I2C_BUS_ERROR,
} i2c_error_t;

typedef struct
{
	unsigned char *Cmd;
	unsigned char *Data;
	unsigned char Address;
	unsigned char CmdLength;
	unsigned char Length;
	i2c_error_t Error;
} i2c_context_t;

#define HAL_I2C_GENERAL_CALL_ADDR 0

static void inline hal_i2c_init_context(i2c_context_t *context, unsigned char addr, void *cmd, unsigned cmd_len)
{
	context->Cmd = cmd;
	context->Address = addr;
	context->CmdLength = cmd_len;
	context->Length = context->Error = 0;
//	event_create(&context->Done, EVENTF_AUTORESET);
}

// prototypes
bool hal_i2c_initialize(unsigned module, unsigned bitrate);
bool hal_i2c_write(unsigned module, i2c_context_t *context, void *data, unsigned length);
bool hal_i2c_read(unsigned module, i2c_context_t *context, void *data, unsigned length);

#endif // SUPPORT_I2C_HAL_H

