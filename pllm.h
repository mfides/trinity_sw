#ifndef PLLM_H
#define PLLM_H

#include "trinity.h"

typedef struct tnt_pllm pllm_t;
    
#define PLLMC_DONE (1UL<<0)
#define PLLMC_BUSY (1UL<<1)
#define PLLMC_READ (1UL<<7)
#define PLLMC_WRITE (1UL<<6)
#define PLLMC_MODE3 (1UL<<4)

typedef struct {
	UBYTE Divide;
	UBYTE Duty;
	SHORT Phase;
} pllm_divider_t;


BOOL pllm_read(pllm_t *pllm);
void pllm_reg2div(ULONG divreg, pllm_divider_t *div);
ULONG pllm_div2reg(const pllm_divider_t *div);
ULONG pllm_filter_lookup(unsigned m);
ULONG pllm_lock_lookup(unsigned m, unsigned part);

#endif // PLLM_H

