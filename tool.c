#include <stdio.h>
#include <stdlib.h>
#include <exec/types.h>
#include <exec/memory.h>
#include "pllm.h"
#include "tool.h"

#include <proto/exec.h>
#include <proto/dos.h>

struct Library *ExpansionBase;

#define DCM_CLOCK 198626UL
#define BCLK_MIN 25000UL
#define BCLK_MAX 56000UL
#define MEMCLK_MAX 133333UL

#define MHZ(k) ((k) / 1000), ((k) % 1000)

static BOOL _get_conf(trinity_conf_t *conf, const pllm_t *pllm);
static void _get_clocks(trinity_clocks_t *clocks, const trinity_conf_t *conf);
static BOOL _validate_clocks(const trinity_clocks_t *clocks);
static BOOL _set_conf(const trinity_conf_t *conf, pllm_t *pllm);
static void _debug_reg(const char *name, ULONG rval);
static unsigned _divr(unsigned divn, unsigned divd);

int main(int argc, char *argv[])
{
	LONG  arg_array[] = { 0, 0, 0 };
	static const char *template = "SPEED/N,040/S,SET/S";

	printf("Trinity control beta (" __DATE__ " " __TIME__ ")\n");
	
	tnt_control_t *tnt = trinity_find();
	if (tnt != NULL)
	{
		pllm_t *pllm = &tnt->pllm;
		if (pllm_read(pllm))
		{
			trinity_conf_t conf;
			BOOL ok = _get_conf(&conf, pllm);
			
			trinity_clocks_t clocks;
			_get_clocks(&clocks, &conf);
			BOOL mode3 = (BOOL)(pllm->Control & PLLMC_MODE3);
			printf("current mem/pclk/bclk = %lu.%03lu/%lu.%03lu/%lu.%03lu MHz (%s)\n", 
				MHZ(clocks.MemClk), MHZ(clocks.PClk), MHZ(clocks.BClk),
				mode3 ? "3X mode" : "2X mode");
		
			if (!ok || !_validate_clocks(&clocks))
			{
				printf("configuration is invalid!\n");
			} 
			else if (argc > 0)
			{
				struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
				
				if (rdargs != NULL)
				{
					BOOL mode040 = (arg_array[1] != 0);
					int req = arg_array[0] ? *(int *)arg_array[0] : 0;
					if (req > 0)
					{
						printf("requested speed is %d Mhz\n", req);
						
						ULONG pclk = req * 1000UL;
						ULONG bclk = (pclk > BCLK_MAX || mode040) ? pclk / 2 : pclk;

						ULONG memclk = bclk * 3;
						if (memclk > MEMCLK_MAX) 
						{
							memclk = bclk * 2;
							mode3 = FALSE;
						}
						else mode3 = TRUE;
#ifdef DEBUG
						printf("requested mem/pclk/bclk = %lu.%03lu/%lu.%03lu/%lu.%03lu MHz (%s)\n", 
							MHZ(clocks.MemClk), MHZ(clocks.PClk), MHZ(clocks.BClk),
							mode3 ? "3X mode" : "2X mode");
#endif	

						conf.MemDiv.Divide = 4;
						conf.DivClk.Divide = 9;
						ULONG target_vco = conf.MemDiv.Divide * memclk;
						ULONG best_m = _divr(conf.DivClk.Divide * target_vco, DCM_CLOCK);
#ifdef DEBUG
						printf("(VCO target = %lu KHz)\n", target_vco);						
#endif
						conf.ClkFBOut.Divide = best_m;
						conf.BClkDiv.Divide = mode3 ? (conf.MemDiv.Divide * 3)
							: (conf.MemDiv.Divide * 2);
						conf.PClkDiv.Divide = conf.BClkDiv.Divide * bclk / pclk;
						
						_get_clocks(&clocks, &conf);

						printf("mem/pclk/bclk = %lu.%03lu/%lu.%03lu/%lu.%03lu MHz (%s)\n", 
							MHZ(clocks.MemClk), MHZ(clocks.PClk), MHZ(clocks.BClk),
							mode3 ? "3X mode" : "2X mode");
							
						if (_validate_clocks(&clocks))
						{
							if (arg_array[2] != 0)
							{
								_set_conf(&conf, pllm);
							}
							else
							{
								printf("ready, use SET to change speed!\n");
							}								
						}
						else printf("new conf is invalid\n");
					}	
				
					FreeArgs(rdargs);
				}
				else printf("could'nt read args\n");
			}
			
		} else printf("pllm read error!\n");
	}
	else printf("autoconfig device not found!\n");


	return 0;
}

static BOOL _get_conf(trinity_conf_t *conf, const pllm_t *pllm)
{
	BOOL ok = TRUE;
	char rname[16];

#ifdef DEBUG
	for(int i = 0; i < 6; i++)
	{
		sprintf(rname, "CLKOUT%d", i);
		_debug_reg(rname, pllm->ClkOut[i]);
	}
	
	_debug_reg("CLKFBOUT", pllm->ClkFBOut);
	_debug_reg("DIVCLK", pllm->DivClk);
	printf("LOCK $%08x $%08x\n", pllm->LockH, pllm->LockL);
	printf("FILTER $%08x\n", pllm->Filter);
#endif

	
	pllm_reg2div(pllm->ClkOut[0], &conf->MemDiv);
	pllm_reg2div(pllm->ClkOut[1], &conf->PClkDiv);
	pllm_reg2div(pllm->ClkOut[2], &conf->BClkDiv);

	if (conf->PClkDiv.Phase != 0 ||
		conf->BClkDiv.Phase != 0)
	{
		printf("pclk and bclk must be phase 0\n");
		ok = FALSE;
	}

	pllm_divider_t clken_div;
	pllm_reg2div(pllm->ClkOut[3], &clken_div);
	if (clken_div.Divide != conf->BClkDiv.Divide ||
		clken_div.Phase != -90)
	{
		printf("clken divider is incorrect\n");
		ok = FALSE;
	}
	
	if (conf->BClkDiv.Divide != conf->PClkDiv.Divide &&
		conf->BClkDiv.Divide != conf->PClkDiv.Divide * 2)
	{
		printf("pclk:blck must 1 or 2\n");
		ok = FALSE; 
	}
	
	if (conf->BClkDiv.Divide != conf->MemDiv.Divide * 2 &&
		conf->BClkDiv.Divide != conf->MemDiv.Divide * 3)
	{
		printf("memclk:bclk must be 2 or 3\n");
		ok = FALSE;
	}
	
	pllm_reg2div(pllm->DivClk, &conf->DivClk);
	pllm_reg2div(pllm->ClkFBOut, &conf->ClkFBOut);
	
	return ok;
}

static BOOL _set_conf(const trinity_conf_t *conf, pllm_t *pllm)
{
	pllm->ClkOut[0] = pllm_div2reg(&conf->MemDiv);
	pllm->ClkOut[1] = pllm_div2reg(&conf->PClkDiv);
	pllm->ClkOut[2] = pllm_div2reg(&conf->BClkDiv);

	pllm_divider_t clken;
	clken.Divide = conf->BClkDiv.Divide;
	clken.Phase = -90;
	clken.Duty = 40;
	pllm->ClkOut[3] = pllm_div2reg(&clken);
	
	pllm->DivClk = pllm_div2reg(&conf->DivClk);
	pllm->ClkFBOut = pllm_div2reg(&conf->ClkFBOut);
	
	pllm->LockH = pllm_lock_lookup(conf->ClkFBOut.Divide, 0);
	pllm->LockL = pllm_lock_lookup(conf->ClkFBOut.Divide, 1);
	pllm->Filter = pllm_filter_lookup(conf->ClkFBOut.Divide);
	
	unsigned mode3;
	if (conf->BClkDiv.Divide == conf->MemDiv.Divide * 3) mode3 = 1;
	else if (conf->BClkDiv.Divide == conf->MemDiv.Divide * 2) mode3 = 0;
	else
	{
		printf("ram:bclk ratio is not 2 or 3!\n");
		return FALSE;
	}
	
	pllm->Control = mode3 ? PLLMC_MODE3 | PLLMC_WRITE : PLLMC_WRITE;
	// NOTE: board should reset now 
	
	return TRUE;
}

static void _get_clocks(trinity_clocks_t *clocks, const trinity_conf_t *conf)
{
	ULONG vco = _divr(DCM_CLOCK * conf->ClkFBOut.Divide, conf->DivClk.Divide);

#ifdef DEBUG
	printf("VCO = %lu x %u:%u = %lu KHz\n", DCM_CLOCK, 
		conf->ClkFBOut.Divide, conf->DivClk.Divide, vco);   
#endif

	clocks->VCO = vco;
	clocks->MemClk = _divr(vco, conf->MemDiv.Divide);
	clocks->PClk = _divr(vco, conf->PClkDiv.Divide); 
	clocks->BClk = _divr(vco, conf->BClkDiv.Divide);
}

static BOOL _validate_clocks(const trinity_clocks_t *clocks)
{
	if (clocks->MemClk > MEMCLK_MAX)
	{
		printf("memclk is too high\n");
		return FALSE;
	}


	if (clocks->BClk < BCLK_MIN || clocks->BClk > BCLK_MAX)
	{
		printf("bclk is out of range!\n");
		return FALSE;
	}
	
	return TRUE;
}

static void _debug_reg(const char *name, ULONG rval)
{
	pllm_divider_t div;
#ifdef DEBUG	
	pllm_reg2div(rval, &div);

	printf("%s=$%08lx (1:%u", name, rval, div.Divide);
	if (div.Phase != 0) printf(" phase %d", div.Phase);
	printf(")\n");
#endif
}

static unsigned _divr(unsigned divn, unsigned divd)
{
	return divd != 0 ? (divn + (divd / 2)) / divd : 0;
}

