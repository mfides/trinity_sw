#include "i2c.h"
#include <support/i2c_hal.h>

#define BASE_CLK 50000000UL // NOTE: usually slower

static tnt_i2c_t *_i2c = NULL;

bool hal_i2c_initialize(unsigned module, unsigned bitrate)
{
	tnt_control_t *tnt = trinity_find();
	if (tnt != NULL)
	{
		_i2c = (tnt_i2c_t *)&tnt->i2c;
		
		unsigned div = ((BASE_CLK >> 4) + (bitrate >> 1)) / bitrate;
		_i2c->DIV = div < 256 ? div : 255; 

		return true;
	}
//	else printf("autoconfig device not found!\n");
	
	return false;
}

static inline bool _start()
{
	_i2c->CON = I2CCON_START;
	for(unsigned i = 0; i < 1000UL; i++)
		if ((_i2c->STA & I2CSTA_OE) != 0) return true;
	return false;
}

static inline bool _stop()
{
	_i2c->CON = 0;	// clear START
	for(unsigned i = 0; i < 1000UL; i++)
		if ((_i2c->STA & I2CSTA_OE) == 0) return true;
	return false;
}

static inline bool _restart()
{
	_i2c->CON = I2CCON_RESTART | I2CCON_START;
	for(unsigned i = 0; i < 1000UL; i++)
		if ((_i2c->STA & I2CSTA_BUSY) == 0) return true;
	return false;
}

static _idle_cnt = 0;

static inline bool _wait_idle()
{
	for(_idle_cnt = 0; _idle_cnt < 10000UL; _idle_cnt++)
		if ((_i2c->STA & I2CSTA_BUSY) == 0) return true;
	return false;
}

static inline bool _transmit1(unsigned char c)
{
	_i2c->DAT = (unsigned int)c;
	_wait_idle();
	return (_i2c->STA & I2CSTA_ACK) != 0 ? true : false;
}

static inline bool _transmit(unsigned char *data, unsigned length)
{
	for(unsigned i = 0; i < length; i++)
		if (!_transmit1(data[i])) return false;
	return true;
}

static unsigned _write(unsigned char addr, unsigned char *data, unsigned length)
{
	if (_transmit1(addr << 1)) // NOTE: LSB 0 for write
	{
		if (_transmit(data, length))
		{
			return I2C_OK;	// OK
		} else return I2C_NACK;	// data NACK
	} else return I2C_NO_RESPONSE; // addr NACK
}

static unsigned _read(unsigned char addr, unsigned char *data, unsigned length)
{
	if (_transmit1((addr << 1) | 1)) // NOTE: LSB 1 for read
	{
		_i2c->CON = I2CCON_ACK | I2CCON_START;
		
		for(unsigned i = 0; i < length; i++)
		{
			if (i == length - 1) 
				_i2c->CON = I2CCON_START;
			_i2c->DAT = 0xff;
			_wait_idle();
			data[i] = _i2c->DAT;	
		}
		return I2C_OK;
	} else return I2C_NO_RESPONSE; // addr NACK
}


bool hal_i2c_write(unsigned module, i2c_context_t *context, void *data, unsigned length)
{
	if (_start())
	{
		context->Error = _write(context->Address, 
			context->Cmd, context->CmdLength);
		
		if (context->Error == I2C_OK &&
			data != (void *)0)
		{
			if (!_transmit(data, length))
				context->Error = I2C_NACK;
		}

		if (!_stop())
			context->Error = I2C_BUS_ERROR;

#ifdef DEBUG
//		printf("idle at %d\n", _idle_cnt);
#endif
	} else context->Error = I2C_BUS_ERROR;
#ifdef DEBUG
//	if (context->Error != I2C_OK) printf("i2c error %u\n", (unsigned)context->Error);
#endif
	
	return (context->Error == I2C_OK);
}

bool hal_i2c_read(unsigned module, i2c_context_t *context, void *data, unsigned length)
{
	bool write_done = 0;
	if (_start())
	{
		context->Error = _write(context->Address, 
			context->Cmd, context->CmdLength);
		
		if (context->Error == I2C_OK &&
			data != (void *)0)
		{
			write_done = true;
			
			if (_restart())
			{
				context->Error = _read(context->Address, data, length);
			} else context->Error = I2C_BUS_ERROR;
		}

		_stop();

#ifdef DEBUG
//		printf("idle at %d\n", _idle_cnt);
#endif
	} else context->Error = I2C_BUS_ERROR;
#ifdef DEBUG
//	if (context->Error != I2C_OK) printf("i2c error %u, %s write\n", 
//		(unsigned)context->Error, write_done ? "after" : "during");
#endif
	
	return (context->Error == I2C_OK);
}


