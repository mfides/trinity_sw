#include <exec/types.h>

#include <hardware/custom.h>
#include <hardware/cia.h>

#include <trinity.h>
#include <musicport/wm8731.h>
#include <support/i2c_hal.h>
#include "loader.h"

extern struct Custom _custom;
extern volatile struct CIA _ciaa;

const i2s_config_t _i2s_config = { .SampleRate = 48000, .SampleBits = 16 };

// #define TRINITY_DEBUG_SIZE 16
// static ULONG _debug[TRINITY_DEBUG_SIZE];

unsigned __load_magic = 0;
unsigned __load_addr = 0;

typedef bool (* stage_entry_t)();

void _main()
{
	struct Custom *hw = &_custom;
	hw->intena = 0x7fff;
	hw->intreq = 0x7fff;
	hw->dmacon = 0x7fff;
	
	hal_i2c_initialize(0, 200000U);
	wm8731_init(&_i2s_config);

	bool chime = true;
	if ((_ciaa.ciapra & 0x40) == 0x40)	// left mouse button (FIXME)
	{
		_custom.bplcon0 = 0x200;
		_custom.bpldat[0] = 0;
#ifdef DEBUG		
		for(unsigned i = 0; i < 0x1000; i++)
		{
			_custom.color[0] = i & 0x0f0;
			for(unsigned volatile j = 0; j < 10; j++);		
		}
#endif
		_custom.color[0] = 0x000;

		unsigned *sec_stage = (unsigned *)0x8800000;
		if (loader_load(512 * 1024, sec_stage, 65536))
		{
			__load_magic = sec_stage[0];
			__load_addr = sec_stage[1];

			if (sec_stage[0] == 0x22222222UL &&
				sec_stage[1] == (unsigned)sec_stage)
			{
				stage_entry_t entry = (stage_entry_t)&sec_stage[2];
				chime = entry();
			}
		}
	}

	if (chime)
	{
		tnt_control_t *tnt = trinity_find();
		// ULONG *debug_ptr = (ULONG *)tnt;
		// for (unsigned i = 0; i < TRINITY_DEBUG_SIZE; i++) _debug[i] = debug_ptr[i];

		// chime sound
		tnt_psg_regs_t *psg = (tnt_psg_regs_t *)&tnt->psg;
		UWORD per_cha = 960;	psg->PER_CHAL = per_cha; psg->PER_CHAH = per_cha >> 8; 
		UWORD per_chb = 762;	psg->PER_CHBL = per_chb; psg->PER_CHBH = per_chb >> 8; 
		UWORD per_chc = 641;	psg->PER_CHCL = per_chc; psg->PER_CHCL = per_chc >> 8; 
		psg->CHSEL = 0x38;	// enable tone ch a,b,c
		psg->VOL_CHA = 0x10; psg->VOL_CHB = 0x10; psg->VOL_CHC = 0x10; // vol, by envelope
		UWORD per_env = 20973;	psg->PER_ENVL = per_env; psg->PER_ENVH = per_env >> 8;
		psg->ENV_SHAPE = 0;	// trigger, shape 0
	}
}


tnt_control_t *trinity_find()
{
	return (tnt_control_t *)0xe88000;
}

