#include <stdio.h>
#include <stdbool.h>

static void _do(const char *filename, unsigned grouping);

int main(int argc, const char *argv[])
{
	const char *filename = NULL;
	unsigned grouping = 0;
	bool error = false;

	for (int i = 1; i < argc; i++)
	{
		const char *arg = argv[i];
		if (arg[0] == '-')
		{
			unsigned arg_gr = 0;
			switch(arg[1])
			{
				case '1':	arg_gr = 1;	break;
				case '2':	arg_gr = 2;	break;
				case '3':	arg_gr = 3;	break;
				case '4':	arg_gr = 4;	break;
				default:
					fprintf(stderr, "unknown option specified\n");
					error = true;
					break;
			}

			if (arg_gr != 0)
			{
				if (grouping == 0) grouping = arg_gr;
				else
				{
					fprintf(stderr, "use only one grouping option\n");
					error = true;
				}
			}
		}
		else if (filename == NULL)
		{
			filename = arg;
		}
	}

	if (!error && grouping != 0 && filename != NULL)
	{
		//printf("file '%s' grouping %d", filename, grouping);
		_do(filename, grouping);
	}
	else
	{
		fprintf(stderr, "\nusage bin2vhex <filename> <-1|-2|-3|-4>\n");
	}
}

static void _do(const char *filename, unsigned grouping)
{
	unsigned char group[4];
	FILE *input = fopen(filename, "rb");
	if (input != NULL)
	{
		int line = 0;
		while(1)
		{
			int read = fread(group, 1, grouping, input);
			for (unsigned i = 0; i < grouping; i++)
			{
				printf("%02x", i < read ? group[i] : (unsigned char)0);
			}
			if (read < grouping) break;
			
			line += read;
			if (line >= 16)	
			{ 
				printf("\n");
				line = 0;
			}
			else printf(" ");
		}
	
		fclose(input);
	}
	else fprintf(stderr, "couldn't open file '%s'", filename);		
}
