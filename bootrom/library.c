#include <exec/execbase.h>
#include <trinity/trinitybase.h>
#include <trinity/audio.h>
#include <libraries/configvars.h>
#include <inline/exec_protos.h>
#include <inline/expansion_protos.h>

#include <trinity.h>
#include <exec/alerts.h>

// NOTE: library ROMTAG uses these
const char library_name[] = "trinity.library";
const char id_string[] = "trinity 1.0 " __AMIGADATE__ "\r\n";
static const char version_tag[] = "$VER: trinity boot rom 1.0 " __AMIGADATE__;


typedef ULONG BPTR;

static __saveds struct Library *_init_library(__reg("a6") struct ExecBase *sys_base, 
		__reg("a0") BPTR seg_list, 
		__reg("d0") struct Library *lib)
{
	lib->lib_Node.ln_Type = NT_LIBRARY;
	lib->lib_Node.ln_Name = (char *)library_name;
	lib->lib_Flags = LIBF_SUMUSED | LIBF_CHANGED;
	lib->lib_Version = 1;
	lib->lib_Revision = 0;
	lib->lib_IdString = (APTR)id_string;

	struct TrinityBase *tb = (struct TrinityBase *)lib;
	tb->SavedSegList = (ULONG)seg_list;

	return lib;
}

static __saveds BPTR _expunge(__reg("a6") struct Library *lib)
{
	if (lib->lib_OpenCnt != 0)
	{
		lib->lib_Flags |= LIBF_DELEXP;
		return 0;
	}

	struct TrinityBase *tb = (struct TrinityBase *)lib;
	BPTR seg_list = tb->SavedSegList;

	struct ExecBase *SysBase = *(struct ExecBase **)4;

	__Remove(SysBase, &lib->lib_Node);
	__FreeMem(SysBase, (char *)lib - lib->lib_NegSize, lib->lib_NegSize + lib->lib_PosSize);
	return seg_list;
}

static void _open(__reg("a6") struct Library *lib)
{
	lib->lib_OpenCnt++;
}

static BPTR _close(__reg("a6") struct Library *lib)
{
	lib->lib_OpenCnt--;

	if (lib->lib_OpenCnt == 0 && (lib->lib_Flags & LIBF_DELEXP))
		return _expunge(lib);

	return 0;
}

#define AUTOCONFIG_MANUF 5110
#define AUTOCONFIG_PROD 61

static __saveds tnt_control_t *_trinity_find()
{
	struct ExecBase *SysBase = *(struct ExecBase **)4;
	struct Library *ExpansionBase;

	tnt_control_t *tc = NULL;

	ExpansionBase = __OpenLibrary(SysBase, "expansion.library", 0L);
	if (ExpansionBase != NULL)
	{
		struct ConfigDev *cd = NULL;
		cd = FindConfigDev(cd, AUTOCONFIG_MANUF, AUTOCONFIG_PROD);
		if (cd != NULL) 
		{
			tc = cd->cd_BoardAddr;
		}

		__CloseLibrary(SysBase, ExpansionBase);
	}

	return tc;
}

static const struct TrinityAudioInfo _core_table[] = {
    { TRA_ID('P','S','G','1'), TRF_CLASSIC | TRF_DIRECT, 0x100, 
	"AY-3-8910", "Miguel Fides", 0x00C0, 16<<2 },
};

static __saveds BOOL _enum_audio_core(__reg("a6") struct Library * , 
    __reg("a0") ULONG *index , __reg("d0") ULONG flags , 
    __reg("a1") struct TrinityAudioInfo *audioInfo)
{
    static const core_count = sizeof(_core_table) / sizeof(struct TrinityAudioInfo);

    ULONG i = *index;
    while(i < core_count)
    {
        const struct TrinityAudioInfo *p = &_core_table[i++];
        *index = i;
        if (0 ==(p->Flags & ~flags))
        {
            *audioInfo = *p;
            return TRUE;
        }
    }
    return FALSE;
}


static const ULONG _library_vectors[] =
{
	(ULONG)_open,
	(ULONG)_close,
	(ULONG)_expunge,
	0,
	(ULONG)_trinity_find,
	(ULONG)_enum_audio_core,
    -1,
};

const ULONG auto_init_tables[] =
{
	sizeof(struct TrinityBase),	/* dataSize */
	(ULONG)_library_vectors,	/* vectors */
	0,						/* structure (for InitStruct()) */
	(ULONG)_init_library,	/* init function */
};


__saveds void hook_init()
{ 
	struct ExecBase *SysBase = *(struct ExecBase **)4;	
	tnt_control_t *tnt = _trinity_find();
	
	if (tnt != NULL)
	{
		if (tnt->gcon == TNT_GCONF_KICKCDIS) 
		{
			//  remove KICKCDIS
			tnt->gcon = 0;
		}
	}
	else
	{
		// DEBUG
		__Alert(SysBase, AN_Unknown + 0xBA5E);
	}
}
