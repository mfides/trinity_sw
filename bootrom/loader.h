#ifndef BOOTROM_LOADER_H
#define BOOTROM_LOADER_H

#include <stdbool.h>

bool loader_load(unsigned offset, void *dest, unsigned length);

#endif

