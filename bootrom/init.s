	section .init
	include "hardware/cia.i"

	machine 68040

romstart:	dc.w $1111
		; sec rom is called very early from kickstart
		; - no hw setting done at all
		; - no stack
		; - overlay still in place
		; - d5 = kickstart checksum (-1)
		; - a5 = return address
start:  bra	setup

	dc.b	"Trinity bootstrap 0.4beta2 "
	dc.b	"Miguel Fides, Luis Peskanov",0

	align 4

setup:  lea	__ciaa,a0
	clr.b	ciapra(a0)	; LED on, OVL off
	move.b	#$3,ciaddra(a0)	; set output LED/OVL pins
	clr.b	ciacra(a0)

	; entire 24-bit amiga space (including chip mem, classic hw resources and roms)
	; NOTE: in early-boot (now) trinity control resides in zorro2 (24-bit) autoconfig memory
	move.l	#$0000C040,d0	; region 00xx_xxxx, E(nabled), S-field 1x (ignore fc2), Non-cachable, serialized  
	movec	d0,itt0		; for instructions
	movec	d0,dtt0		; for data
	; trinity local fast ram (64/128Mbyte) is always mapped at 0800_0000
	move.l	#$0807C020,d0	; region 08xx_xxxx, E(nabled), S-field 1x (ignore fc2), Cachable, copy-back  
	movec	d0,itt1		; not really needed for insts
	movec	d0,dtt1

	cinva	bc	; both caches
	; DC=1 (enable data cache), IC=1 (enable inst cache)
	move.l	#$80008000,d0
	movec	d0,cacr

	lea	$80400,a4
	move.l	a4,sp
	movem.l	d5/a5,-(sp)

	lea	__data_start__,a0
	lea	__data_end__,a1
	lea	__data_load_start__,a2
.copy1  cmp.l	a1,a0
	bcc	.done1
	move.l	(a0)+,(a2)+
	bra     .copy1

.done1  jsr     __main

	cpusha	bc

	clr.l	d0
	movec	d0,cacr
	cinva	bc

	movem.l	(sp)+,d5/a5


dis_fpu		move.l	$10,d3		; illegal instr vector
		pea	.do040(pc)
		move.l	(sp)+,$10

	machine 68060

		movec	pcr,d0		; read PCR register (060 only)
		and.l	#2,d0		; get DFP
		cmp.l	#2,d0
		beq	.skip		; fpu already disabled

		fnop
		move.l	#2,d0		; disable fpu
		movec	d0,pcr
		bra	.skip

	machine 68040

.do040		lea	$e88000,a0	; trinity base (unconfigured)
		or.l	#2,$30(a0)	; set GCON.KCDIS (disable kickstart cache)

.skip		move.l	d3,$10		; restore vector

	jmp	(a5)


	include "exec/nodes.i"
	include "exec/resident.i"

VERSION		equ	1
LIB_PRIORITY	equ	0
HOOK_PRIORITY	equ	0

hook_romtag	dc.w	RTC_MATCHWORD
		dc.l	hook_romtag
		dc.l	hook_romtag_end
		dc.b	RTF_COLDSTART
		dc.b	VERSION
		dc.b	NT_UNKNOWN
		dc.b	HOOK_PRIORITY
		dc.l	.hook_name
		dc.l 	.hook_name
		dc.l	_hook_init

.hook_name	dc.b	"trinity.hook",13,10,0
		align	2
hook_romtag_end	

lib_romtag	dc.w	RTC_MATCHWORD
		dc.l	lib_romtag
		dc.l	lib_romtag_end
		dc.b	RTF_AUTOINIT
		dc.b	VERSION
		dc.b	NT_LIBRARY
		dc.b	LIB_PRIORITY
		dc.l	_library_name
		dc.l	_id_string
		dc.l	_auto_init_tables
lib_romtag_end

