#include "loader.h"
#include <trinity.h>
#include <spi.h>

#define BURST_READ 1024

static void _fast_write(struct tnt_spi *spi, const char *buf, unsigned length);
static void _fast_read(struct tnt_spi *spi, char *buf, unsigned length);
static inline _flush() = "\tcpusha\tbc\n";

bool loader_load(unsigned offset, void *dest, unsigned length)
{
	tnt_control_t *tnt = trinity_find();
	struct tnt_spi *spi = &tnt->spi;

	spi->DIV = 2;	// max speed supported @ max bus clock
	unsigned char con = SPICON_CPHA | SPICON_CPOL;
	spi->CON = con;

	unsigned done = 0;
	while(done < length)
	{
		unsigned char cmd[4] = { 0x03, offset >> 16, offset >> 8, offset };

		spi->CON = con | SPICON_ENABLE;
		_fast_write(spi, cmd, sizeof(cmd));
		_fast_read(spi, (unsigned char *)dest + done, BURST_READ);
		spi->CON = con;
		done += BURST_READ;
		offset += BURST_READ;
	}
	_flush();
	return true;
}

static void _fast_write(struct tnt_spi *spi, const char *buf, unsigned length)
{
	for (unsigned i = 0; i < length; i++)
	{
		spi->DAT = buf[i];
		while(spi->STA & SPISTA_BUSY);
//		buf[i] = _spi->DAT;
	}
}

static void _fast_read(struct tnt_spi *spi, char *buf, unsigned length)
{
	for (unsigned i = 0; i < length; i++)
	{
		spi->DAT = 0;
		while(spi->STA & SPISTA_BUSY);
		*buf++ = spi->DAT;
	}
}
