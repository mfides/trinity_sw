#ifndef TRINITY_FLASH_H
#define TRINITY_FLASH_H

typedef struct
{
	unsigned char ManufacturerId;
	unsigned char DeviceId[2];
	unsigned char ExtBytes;
} flash_info_t;

#endif // TRINITY_FLASH_H
