#include "systime.h"
#include <stdio.h>
#include <stdbool.h>

#include <exec/io.h>
#include <devices/timer.h>

#include <proto/exec.h>

static struct timerequest _tio;

static bool _open_device()
{
    struct MsgPort *mp = CreateMsgPort();
    if (mp != NULL)
    {
        struct IORequest *io = &_tio.tr_node;
        struct Message *mn = &io->io_Message;
        mn->mn_ReplyPort = mp;
        mn->mn_Length = sizeof(struct timerequest);
        io->io_Device = NULL;
        io->io_Unit = NULL;
        io->io_Flags = 0;
        
        int error = OpenDevice("timer.device", UNIT_VBLANK, io, 0L);
        if (error == 0)
	        return true; 
    
        DeleteMsgPort(mp);
    }
    return false;
}

static void _close_device()
{
	struct IORequest *io = &_tio.tr_node;
	CloseDevice(io);
	
    struct MsgPort *mp = io->io_Message.mn_ReplyPort;
    DeleteMsgPort(mp);
}

static bool _do_io(ULONG cmd)
{
	_tio.tr_node.io_Command = cmd;
	int error = DoIO(&_tio.tr_node);
    return error == 0;
}

BOOL systime_get(ULONG *ptime)
{
    if (_open_device())
    {
		if (_do_io(TR_GETSYSTIME))
		{
			*ptime = _tio.tr_time.tv_secs;
		
			return TRUE;
		}
		else printf("timer command failed|\n");

        _close_device();
    } else printf("Failed to open timer.device\n");
	return FALSE;
}


BOOL systime_set(ULONG time)
{
    if (_open_device())
    {
		_tio.tr_time.tv_secs = time;
		_tio.tr_time.tv_micro = 0L;
		
		if (_do_io(TR_SETSYSTIME))
		{
			return TRUE;
		}
		else printf("timer command failed|\n");

        _close_device();
    } else printf("Failed to open timer.device\n");
	return FALSE;
}



