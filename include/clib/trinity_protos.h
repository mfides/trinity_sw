/* Automatically generated header (sfdc 1.12)! Do not edit! */

#ifndef CLIB_TRINITY_PROTOS_H
#define CLIB_TRINITY_PROTOS_H

/*
**   $VER: trinity_protos.h $VER:trinity_lib.sfd 0.4 (27.1.2025) $VER:trinity_lib.sfd 0.4 (27.1.2025)
**
**   C prototypes. For use with 32 bit integers only.
**
**   Copyright (c) 2001 Amiga, Inc.
**       All Rights Reserved
*/

#include <trinity/trinitybase.h>
#include <trinity/audio.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/*--- functions in V0 or higher ---*/

/* "trinity.library" */
APTR TrinityFind(void);
BOOL EnumAudioCore(ULONG *index, ULONG flags, struct TrinityAudioInfo *audioInfo);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CLIB_TRINITY_PROTOS_H */
