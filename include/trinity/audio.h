#ifndef TRINITY_AUDIO_H
#define TRINITY_AUDIO_H

#include <exec/types.h>

#define TRA_ID(a, b, c, d) (((a) << 24) | ((b) << 16) | ((c) << 8) | (d))

#define TRF_CLASSIC (1<<0)
#define TRF_DIRECT  (1<<1)
#define TRF_STEREO  (1<<2)
#define TRF_VIRTUAL (1<<3)
#define TRF_REQ_ACTIVATION  (1<<8)

struct TrinityAudioInfo 
{
    ULONG Id;               // 4 byte compatibility id
    UWORD Flags;
    UWORD Version;          // hi byte major, lo byte minor version
    const char Name[16];
    const char Author[20];
    UWORD Offset;
    UWORD Size;
};

#endif // TRINITY_AUDIO_H
