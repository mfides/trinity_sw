/* Automatically generated header (sfdc 1.12)! Do not edit! */

#ifndef _INLINE_TRINITY_H
#define _INLINE_TRINITY_H

#ifndef _SFDC_VARARG_DEFINED
#define _SFDC_VARARG_DEFINED
#ifdef __HAVE_IPTR_ATTR__
typedef APTR _sfdc_vararg __attribute__((iptr));
#else
typedef ULONG _sfdc_vararg;
#endif /* __HAVE_IPTR_ATTR__ */
#endif /* _SFDC_VARARG_DEFINED */

APTR __TrinityFind(__reg("a6") struct TrinityBase * ) = "\tjsr\t-30(a6)";
#define TrinityFind() __TrinityFind(TrinityBase)

BOOL __EnumAudioCore(__reg("a6") struct TrinityBase * , __reg("a0") ULONG *index , __reg("d0") ULONG flags , __reg("a1") struct TrinityAudioInfo *audioInfo ) = "\tjsr\t-36(a6)";
#define EnumAudioCore(index, flags, audioInfo) __EnumAudioCore(TrinityBase, (index), (flags), (audioInfo))

#endif /* !_INLINE_TRINITY_H */
