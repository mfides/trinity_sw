#ifndef TRINITY_BASE_H
#define TRINITY_BASE_H

#include <stdbool.h>
#include <exec/libraries.h>

struct TrinityBase
{
    struct Library LibNode;
    ULONG SavedSegList;
};


#endif  // TRINITY_BASE_H
