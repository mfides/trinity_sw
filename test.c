#include <stdio.h>
#include <stdbool.h>

#include <exec/types.h>
#include <exec/memory.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include <trinity/audio.h>
#include <trinity/trinitybase.h>
#include <trinity/trinity_protos.h>
#include "trinity.h"


static void _show_audio_info(struct TrinityAudioInfo *tai);

struct Library *ExpansionBase;
struct Library *UtilityBase;
struct TrinityBase *TrinityBase;

int main(int argc, char *argv[])
{
	LONG  arg_array[] = { 0, 0 };
	static const char *template = "ENUM/S,SAVE/S";

	printf("Trinity.library test tool\n");

	TrinityBase = (struct TrinityBase *)OpenLibrary("trinity.library", 1L);
	if (TrinityBase == NULL)
	{
		printf("library not found. Update your core..\n");
		return -1;
	}
 	
	UtilityBase = OpenLibrary("utility.library", 0L);
	if (UtilityBase != NULL)
	{

		if (argc > 0)
		{
			struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
		
			if (rdargs != NULL)
			{
				if (arg_array[0] != 0) // ENUM
				{
					printf("Enumerating audio core..\n");
					ULONG index = 0;
					struct TrinityAudioInfo tai;
					while(EnumAudioCore(&index, -1L, &tai))
						_show_audio_info(&tai); 

					printf("done!\n");
				}
				else if (arg_array[1] != 0) // SAVE
				{
					printf("SAVE\n");
				}
				else 
				{
					tnt_control_t *tnt = (tnt_control_t *)TrinityFind();
					printf("Control at 0x%lx\n", (ULONG)tnt);
					if (tnt != NULL)
					{
						ULONG gcon = tnt->gcon;
						printf("GCON register=0x%lx\n", gcon);

						if (gcon & TNT_GCONF_KICKCDIS)
							printf("kickstart caching is disabled!\n");
							 
						if (gcon & TNT_GCONF_CHIPCDIS)
							printf("chip ram caching is disabled!\n");
					}
				}

			} else printf("Use %s\n", template);
		}
	
		CloseLibrary((struct Library *)UtilityBase);
	} else printf("couldn't open utility.library\n");
	
	CloseLibrary((struct Library *)TrinityBase);
	
	return 0;
}

static void _show_audio_info(struct TrinityAudioInfo *tai)
{
	printf("core: %s version %d.%d (%s)\n", tai->Name, 
		tai->Version >> 8, tai->Version & 0xff,
		tai->Author);
	printf("id: 0x%04lx flags: ", tai->Id);
	if (tai->Flags & TRF_CLASSIC) printf("CLASSIC ");
	if (tai->Flags & TRF_DIRECT) printf("DIRECT ");
	if (tai->Flags & TRF_STEREO) printf("STEREO ");
	if (tai->Flags & TRF_VIRTUAL) printf("VIRTUAL ");
	printf("offset: +$%x (%u bytes)\n", tai->Offset, tai->Size);
}



