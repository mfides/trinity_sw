#ifndef TRINITY_I2C_H
#define TRINITY_I2C_H

#include "trinity.h"

typedef struct tnt_i2c tnt_i2c_t;

#define I2CCON_RESTART (1UL<<2)
#define I2CCON_ACK (1UL<<1)
#define I2CCON_START (1UL<<0)

#define I2CSTA_BUSY (1UL<<7)
#define I2CSTA_OE (1UL<<6)
#define I2CSTA_ACK (1UL<<0)



#endif // TRINITY_I2C_H

