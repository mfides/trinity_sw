;APS00000000000000000000000000000000000000000000000000000000000000000000000000000000
start	lea 	$40000000,a5
	lea	$c0(a5),a4	; psg

	move.w	#855,d0		; ch1 per
	move.l	d0,$0(a4)
	lsr.w	#8,d0
	move.l	d0,$4(a4)

	move.w	#679,d0		; ch2 per
	move.l	d0,$8(a4)
	lsr.w	#8,d0
	move.l	d0,$c(a4)

	move.w	#571,d0		; ch3 per
	move.l	d0,$10(a4)
	lsr.w	#8,d0
	move.l	d0,$14(a4)

	move.l	#$38,$1c(a4)

	move.b 	#$1F,d0		; max with env
	move.l	d0,$20(a4)	; ch1 vol
	move.l	d0,$24(a4)	; ch2 vol
	move.l	d0,$28(a4)	; ch3 vol

	move.w	#20973,d0
	move.l	d0,$2c(a4)	; env per l
	ror.w	#8,d0
	move.l	d0,$30(a4)	; env per h

	move.l	#$00,$34(a4)
	rts
	
