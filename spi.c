#include "spi.h"
#include <support/spi_hal.h>
#ifdef DEBUG
#include <stdio.h>
#endif

#define BASE_CLK 50000000UL // NOTE: usually slower

static tnt_spi_t *_spi = NULL;
static unsigned char _con = 0;

bool hal_spi_initialize(unsigned module, unsigned bitrate)
{
	tnt_control_t *tnt = trinity_find();
	if (tnt != NULL)
	{
		_spi = (tnt_spi_t *)&tnt->spi;
		unsigned div = ((BASE_CLK >> 1) + (bitrate >> 1)) / bitrate;
		_spi->DIV = div < 256 ? div : 255; 
#ifdef DEBUG
//		printf("using spi divider = %u\n", _spi->DIV);		
#endif
		_con = SPICON_CPHA | SPICON_CPOL;
		_spi->CON = _con;

		return true;
	}
#ifdef DEBUG
//	else printf("autoconfig device not found!\n");
#endif
	
	return false;
}

bool hal_spi_transmit(unsigned module, const unsigned char *outbuf, unsigned char *inbuf, unsigned length)
{
	if (_spi != NULL)
	{
		if (_spi->STA & SPISTA_BUSY)
			return false;

		_spi->CON = _con | SPICON_ENABLE;
		if (_spi->CON & SPICON_ENABLE)
		{
			for (unsigned i = 0; i < length; i++)
			{
				_spi->DAT = outbuf[i];
				while(_spi->STA & SPISTA_BUSY);
				inbuf[i] = _spi->DAT;
			}
			
			_spi->CON = _con;
			return true; 
		}
	}
	return false;
}

