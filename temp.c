#include <stdio.h>
#include <stdbool.h>

#include <exec/execbase.h>
#include <exec/types.h>
#include <exec/memory.h>
#include <support/i2c_hal.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

struct Library *ExpansionBase;
struct Library *UtilityBase;

#define ADC121_ADDR 0x54

static bool _read_pcr(unsigned *pval);
static bool _read_regs(unsigned char *buf, unsigned start, unsigned len);
static bool _read_tempf(int *ptemp10);

int main(int argc, char *argv[])
{
	LONG  arg_array[] = { 0, 0 };
	static const char *template = "LOAD/S,SAVE/S";

	printf("Trinity temperature monitorl\n");

	UtilityBase = /*(struct UtilityBase *)*/OpenLibrary("utility.library", 0L);
	if (UtilityBase != NULL)
	{
		unsigned pcr;
		unsigned char cpu_rev = 0;

		if (_read_pcr(&pcr))
		{
#ifdef DEBUG
			printf("pcr = %08x\n", pcr);
#endif
			const char *model;
			switch(pcr>>16)
			{
				case 0x430:	model = "68060";	break;
				case 0x431:	model = "68EC060/LC060";	break;
				default:	model = "???";
			}
			cpu_rev = (pcr>>8) & 15;
			printf("%s (rev %u)\n", model, cpu_rev);
			
			if (cpu_rev != 6)
			{
				printf("WARNING: default values for calibration may be (very) wrong!");
			}
		}
		else printf("not an 060!\n");

		int temp10;

		if (_read_tempf(&temp10))
		{
			printf("temp read: %d.%d degC\n", temp10/10, temp10%10);

			if (argc > 0)
			{
				struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
					
				if (rdargs != NULL)
				{
/*					if (arg_array[0] != 0) // Load rtc into amiga
					{
						printf("load rtc to systemtime...\n");
						if (systime_set(date))
						{
							printf("done!\n");
						}
						else printf("error\n");
					}
					else if (arg_array[1] != 0) // Save amiga into rtc
					{
						printf("save current systemtime to rtc...\n");
						if (systime_get(&date))
						{
							_debug_amiga_date(date);
							Amiga2Date(date, &cd);

							printf("clockdata: time %02u:%02u:%02u date %u-%s-%u (%s)\n",
								cd.hour, cd.min, cd.sec,
								cd.mday, _month(cd.month), cd.year, _wday(cd.wday)); 
							
							if (_write_rtc(&cd))
							{
								printf("done!\n");
							}
							else printf("i2c port error\n");

						} else printf("couldn't get current systime\n");
					}*/
				} else printf("Use %s\n", template);
			}
		} else printf("i2c port error\n");
	
		CloseLibrary((struct Library *)UtilityBase);
	} else printf("couldn't open utility.library\n");
	
	return 0;
}

static unsigned _read_pcr_asm() = "\tmachine 68060\n" "\tmovec pcr,d0\n" "\trte\n";

static unsigned _read_pcr_super()
{
	return _read_pcr_asm();
}

static bool _read_pcr(unsigned *pval)
{
	struct ExecBase *exec_base = *(struct ExecBase **)4UL;
	if (exec_base->AttnFlags & AFF_68060)
	{
		unsigned pcr = Supervisor((void *)_read_pcr_super);
		*pval = pcr;
		return true;
	}
	return false;
}


// Rpu = 1K in Trinity
// Rth = R0 + Rcoeff(T-T0 degc)
// V = Vcc * Rth / (Rth + Rpu)
// c = Rth / (Rth + Rpu)
static int _cnt2temp8(unsigned c12)
{
	const int rpu = 1000; // ohm
	const int r0 = 780;	// ohm
	const int t0 = 25;	// degc
	const int rcoeff = 28; // 2.8*10

	// Vadc = Vcc * (cnt/4096)
	int delta = (((c12 - 4096) * r0) + (c12 * rpu)) * 10 * 256 / (rcoeff * (4096 - c12));
	return delta + (t0 * 256);
}

static bool _read_temp(unsigned short *ptemp)
{
	unsigned char temp[2];
	if (_read_regs(temp, 0, 2))
	{
#ifdef DEBUG
		printf("raw bytes: %02x %02x\n", temp[0], temp[1]);
#endif	
		unsigned short value = ((unsigned short)temp[0] << 8) + temp[1];
		*ptemp = value & 0xFFF; 
		return true;
	}
	return false;
}

static bool _read_tempf(int *ptemp10)
{
	unsigned short adc;
	if (_read_temp(&adc))
	{
#ifdef DEBUG
		printf("raw value: %u/4096 (%d mV nom.)\n", adc, (adc * 3300) >>12);
#endif	
		int temp8 = _cnt2temp8(adc);
#ifdef DEBUG
		printf("temp8: %d/256=%d\n", temp8, temp8 >> 8);
#endif	
		*ptemp10 = (temp8 * 10) >> 8;
		return true;
	}
	return false;
}

static bool _read_regs(unsigned char *buf, unsigned start, unsigned len)
{
	i2c_context_t i2c;

	if (hal_i2c_initialize(0, 200000UL))
	{
		unsigned char cmd[] = { start };	// reg pointer

		hal_i2c_init_context(&i2c, ADC121_ADDR, cmd, sizeof(cmd));
		bool done = hal_i2c_read(0, &i2c, 
			buf, len);
		if (done)
		{
			return true;
		} else printf("i2c read error!\n");
	} else printf("i2c init failed\n");
	return false;
}


