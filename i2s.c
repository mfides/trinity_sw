#include "i2s.h"
#include "dma.h"
#include <support/i2s_hal.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <assert.h>

#include <proto/exec.h>

ULONG __i2s_sig_mask;

static tnt_i2s_t *_i2s = NULL;
static unsigned _sample_rate = 0;
static i2s_double_buffer_t *_dbuf_out = NULL;

//static volatile _state_out = I2S_STATE_STOP;

static void _dma_handler(unsigned module);

bool hal_i2s_init_slave(const i2s_config_t *cfg)
{
	if (cfg == NULL)
		return false;
	
	tnt_control_t *tnt = trinity_find();
	if (tnt != NULL)
	{
		_i2s = &tnt->i2s;

		_i2s->CON = 0;
		return true;
	}
}

ULONG hal_i2s_start(i2s_double_buffer_t *buf)
{
	static dma_interrupt_t _di;

	if (buf != NULL)
	{
		Disable();
	
		ULONG sig_mask = dma_add_int_handler(0, &_di, _dma_handler);

		_dbuf_out = buf;
		_dbuf_out->ActiveIndex = 0;
//		_state_out = I2S_STATE_RUN;
		dma_start(0, _dbuf_out->Buffer[0], _dbuf_out->Length, 
			DMACON_INTEN | DMACON_LOOP | DMACON_PSEL_DAC);

		_i2s->CON |= I2SCON_EN_DAC;
	
		Enable();

#ifdef DEBUG
//		printf("dma_interrupt=$%lx\n", (ULONG)&_di);
#endif

		return sig_mask;
	}
	return 0;
}

void hal_i2s_stop()
{
//	assert(_i2s != NULL);

	dma_rem_int_handler(0);
	dma_stop(0);
	_i2s->CON = 0;	// FIXME: disable only dac
}

static void _dma_handler(unsigned module)
{
	if (_i2s != NULL && _dbuf_out != NULL)
	{
		_dbuf_out->ActiveIndex ^= 1;
		void *ptr = _dbuf_out->Buffer[_dbuf_out->ActiveIndex & 1];
		dma_reload(module, ptr, _dbuf_out->Length);
	}
}



