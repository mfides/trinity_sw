#ifndef TRINITY_SPI_H
#define TRINITY_SPI_H

#include "trinity.h"

typedef struct tnt_spi tnt_spi_t;

#define SPICON_ENABLE (1UL<<0)
#define SPICON_CPHA (1UL<<4)
#define SPICON_CPOL (1UL<<5)

#define SPISTA_BUSY (1UL<<0)

#endif // TRINITY_SPI_H

