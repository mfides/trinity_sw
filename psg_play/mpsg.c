/* 
 * MPSG	PLAYER 
 * PSG	Player for Amiga PSG Core (YM2149f)	on Trinity Accelerator Card	
 * Original code by Armando Pérez
 * Trinity code by Miguel Fides
 */

#include "psg_hal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef	unsigned char byte;
const char HEADER[]	= {0x50, 0x53, 0x47, 0x1A};	// PSG + #1A
#define	FOFFSET	0x10 //	File data offset												
#define	SET_PSG(r,v) _psg_base->reg[r] = (v)

static inline bool wait_vbl(void) { return psg_hal_wait_vbl(); }

void logo (void);
void usage (void);

static void _play(byte *buffer,	int	size);


int	main (int argc,	char *argv[]) {

	FILE*	finput;
	char*	filename;
	byte*	buffer;
	int		sizefile;
	int		sizeread;
	int		retval = -1;

	logo();
	if (argc < 2) {
		usage();
		return 0;
	}

	if (psg_hal_initialize())
	{
		filename = argv[1];
		if ((finput	= fopen	(filename,"rb")) !=	NULL )
		{
			fseek(finput, 0, SEEK_END);
			sizefile = ftell(finput);
			fseek(finput, 0, SEEK_SET);
			fprintf(stdout,"Size of	%s -> %i\n",filename, sizefile);
		
			buffer = (byte*) malloc(sizefile);
			if (buffer != NULL)	
			{
				int	sizeread = fread(buffer, sizeof(byte), sizefile, finput);
			
				if (sizeread ==	sizefile) 
				{
					if (memcmp(buffer, HEADER, sizeof(HEADER)) == 0)
					{	
						memset(_psg_base, 0, sizeof(psg_regs_t));

						_play(buffer, sizefile);
						
						printf("done!\n");
						retval = 0;	// allright
					}
					else printf("ERROR!! %s	is not a .psg file!! \n\n",filename);
				}
				else printf("ERROR!! Error reading %s \n\n",filename);
				
				free(buffer);
			}
			else printf("Error allocating memory ... \n");

			fclose(finput);
		}
		else printf("ERROR!! Can't open file '%s'\n\n",filename);
		
		psg_hal_destroy();
	}
	else printf("ERROR!! Trinity psg init failed!\n\n");

	return retval;
}

static void _play(byte *buffer,	int	size)
{		
	byte	reg;
	byte	value;
	int		idx	= FOFFSET;

	while (idx < size) 
	{
		reg	= buffer[idx++];
		
		// EOF?
		if (reg	== 0xFD)
			break;
		
		if	(reg !=	0xFF) 
		{
			value =	buffer[idx++];
			//printf("SET_PSG(%x, %x)\n", reg & 0xFF, value & 0xFF);
			SET_PSG(reg, value);
		}
		else // reg was an FF
		{
			if (!wait_vbl())
				break;
		}
	}
	
	SET_PSG(7, 0x3F); // mute
}


void logo (void) 
{
	printf("MPSG player 2024 Armando Perez, Miguel Fides\n");
	printf("Play raw .psg MSX files on Trinity Card\n\n");	  
}



void usage(void) 
{
	printf("Usage: mpsg	file\n\n");
	printf("Example: mpsg mazeof.psg\n\n");
}


