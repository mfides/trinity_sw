#ifndef PSG_HAL_H
#define PSG_HAL_H

#include <stdbool.h>

typedef struct
{
    volatile unsigned int reg[16]; 
} psg_regs_t;

extern psg_regs_t *_psg_base;

bool psg_hal_initialize();
bool psg_hal_wait_vbl();
void psg_hal_destroy();

#endif // PSG_HAL_H

