#include "psg_hal.h"
#include <trinity.h>
#include <stdio.h>
#include <stdbool.h>

#include <exec/types.h>
#include <exec/memory.h>
#include <exec/interrupts.h>
#include <hardware/intbits.h>

typedef struct 
{
	int SigBit;
	struct Task *ServerTask;
	struct Interrupt Interrupt;
} my_interrupt_t;


#include <proto/exec.h>
#include <proto/dos.h>

struct Library *ExpansionBase;

psg_regs_t *_psg_base = NULL;

static const ULONG _int_num = INTB_VERTB;
static my_interrupt_t _interrupt;
static ULONG _sm_vblank;

static ULONG _add_int_handler(my_interrupt_t *mi);
static void _rem_int_handler(my_interrupt_t *mi);

bool psg_hal_initialize()
{
	tnt_control_t *tnt = trinity_find();
	if (tnt != NULL)
	{
		_psg_base = (psg_regs_t *)&tnt->psg;
		_sm_vblank = _add_int_handler(&_interrupt); 
		
		if (_sm_vblank != 0UL)
		{
			struct Task *this_task = FindTask(0);
			SetTaskPri(this_task, 25);
			
			return true;
		}
	}
	return false;
}

bool psg_hal_wait_vbl()
{
	ULONG mask = _sm_vblank;
	if (mask != 0)
	{
		ULONG done = Wait(mask | SIGBREAKF_CTRL_C);
		return done == mask;
	}
	return false;
}

void psg_hal_destroy()
{
	_rem_int_handler(&_interrupt);
}



static ULONG __amigainterrupt __saveds _int_handler(__reg("a1") void *data)
{
	my_interrupt_t *mi = (my_interrupt_t *)data;
	if (mi != NULL && mi->ServerTask != NULL)
	{
//		if (mi->Handler != NULL)
//			mi->Handler(mi);

		ULONG sig_mask = 1UL << mi->SigBit;
		Signal(mi->ServerTask, sig_mask);
	}
	return 0;
}


static ULONG _add_int_handler(my_interrupt_t *mi)
{
	mi->SigBit = AllocSignal(-1);
	if (mi->SigBit != -1)
	{
		ULONG sig_mask = 1UL << mi->SigBit;
		mi->ServerTask = FindTask(NULL);
//		mi->Handler = handler;
		
		struct Interrupt *intr = &mi->Interrupt;
		intr->is_Node = (struct Node) {.ln_Pri = 0 };
		intr->is_Data = (APTR)mi;
		intr->is_Code = (void (*)())_int_handler;
		AddIntServer(_int_num, intr);
			
		return sig_mask;
	}
	return 0UL;
}


static void _rem_int_handler(my_interrupt_t *mi)
{
	if (mi->SigBit != -1)
	{
		RemIntServer(_int_num, &mi->Interrupt);
		FreeSignal(mi->SigBit);
	}
}

void _chkabort()
{
}

