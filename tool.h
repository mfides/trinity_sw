#ifndef TRINITY_TOOL_H
#define TRINITY_TOOL_H

#include "pllm.h"

typedef struct
{
	pllm_divider_t MemDiv, PClkDiv, BClkDiv;
	pllm_divider_t DivClk, ClkFBOut;
} trinity_conf_t;

typedef struct
{
	ULONG VCO;
    ULONG MemClk, PClk, BClk;
} trinity_clocks_t;



#endif // TRINITY_TOOL_H


