#include "dma.h"
//#include <assert.h>

#include <exec/types.h>
#include <hardware/intbits.h>

#include <proto/exec.h>

static const ULONG _int_num = INTB_EXTER;
static tnt_control_t *_tnt = NULL;

void dma_start(unsigned module, void *src, unsigned short length, ULONG control)
{
	if (_tnt == NULL)
		_tnt = trinity_find();
		
	if (_tnt != NULL)
	{
		tnt_dma_t *dma = &_tnt->dma[module];
		dma->CON = (control & 0xf0) | DMACON_DONE | DMACON_START;
		dma->SRC = (unsigned int)src;
		dma->LEN = length;	// writing length starts DMA  
	}
}

void dma_reload(unsigned module, void *src, unsigned short length)
{
	if (_tnt == NULL)
		_tnt = trinity_find();

	if (_tnt != NULL)
	{
		tnt_dma_t *dma = &_tnt->dma[module];

		dma->SRC = (unsigned int)src;	
		dma->LEN = length;	// it has no effect when busy, until reload 
	}
}

void dma_stop(unsigned module)
{
	if (_tnt == NULL)
		_tnt = trinity_find();

	if (_tnt != NULL)
	{
		tnt_dma_t *dma = &_tnt->dma[module];
		dma->CON = (dma->CON & (DMACON_PSEL_MASK | DMACON_START));
		dma->LEN = 0;	// FIXME: this doesn't stop dma!
	}
}

static ULONG __amigainterrupt __saveds _int_handler(__reg("a1") void *data)
{
	dma_interrupt_t *di = (dma_interrupt_t *)data;
	if (di != NULL && di->Module < TRINITY_DMA_COUNT)
	{
		tnt_dma_t *dma = &_tnt->dma[di->Module];
		
		unsigned int dmacon = dma->CON; 
		if (dmacon & DMACON_START)
		{
			if (di->Handler != NULL)
				di->Handler(di->Module);

			ULONG sig_mask = 1UL << di->SigBit;
			Signal(di->ServerTask, sig_mask);

			dma->CON = dmacon & (0xF0 | DMACON_START); // clear START
		}
	}
	return 0;
}


static dma_interrupt_t *_table[TRINITY_DMA_COUNT];

ULONG dma_add_int_handler(unsigned module, dma_interrupt_t *di, dma_handler_t handler)
{
//	assert(di != NULL && handler != NULL);
	
	if (module < TRINITY_DMA_COUNT)
	{
		di->SigBit = AllocSignal(-1);
		if (di->SigBit != -1)
		{
			ULONG sig_mask = 1UL << di->SigBit;
			di->ServerTask = FindTask(NULL);
			di->Handler = handler;
			
			struct Interrupt *intr = &di->Interrupt;
			intr->is_Node = (struct Node) {.ln_Pri = 0 };
			intr->is_Data = (APTR)di;
			intr->is_Code = (void (*)())_int_handler;
			AddIntServer(_int_num, intr);
				
			_table[module] = di;
			return sig_mask;
		}
	}	
	return 0;
}


void dma_rem_int_handler(unsigned module)
{
	if (module < TRINITY_DMA_COUNT)
	{
		dma_interrupt_t *di = _table[module];
//		assert(di != NULL);
		
		if (di->SigBit != -1)
		{
			RemIntServer(_int_num, &di->Interrupt);
			FreeSignal(di->SigBit);
		}
		
		_table[module] = NULL;
	}
}

