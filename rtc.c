#include <stdio.h>
#include <stdbool.h>
#include "systime.h"

#include <exec/types.h>
#include <exec/memory.h>
#include <support/i2c_hal.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

struct Library *ExpansionBase;
struct UtilityBase *UtilityBase;

#define DS3231_ADDR 0x68

static unsigned char _rtc_data[16];
static char _strday[32];
static char _strdate[64];
static char _strtime[32];

static bool _read_rtc(struct ClockData *cd);
static bool _write_rtc(struct ClockData *cd);
static const char *_month(unsigned month);
static const char *_wday(unsigned wday);
static unsigned _idiv(ULONG *ap, ULONG b);
static void _debug_amiga_date(ULONG date);

int main(int argc, char *argv[])
{
	LONG  arg_array[] = { 0, 0 };
	static const char *template = "LOAD/S,SAVE/S";

	printf("Trinity/DS3132 rtc tool\n");
 	
	UtilityBase = (struct UtilityBase *)OpenLibrary("utility.library", 0L);
	if (UtilityBase != NULL)
	{
#ifdef DEBUG	
		for(unsigned i = 0; i < sizeof(_rtc_data); i++) _rtc_data[i] = 0xaa;	
#endif		
		struct ClockData cd;

		if (_read_rtc(&cd))
		{
#ifdef DEBUG	
			for(unsigned i = 0; i < sizeof(_rtc_data); i++)
				printf("%02x ", _rtc_data[i]);
			printf("\n");
#endif

			// print clockdata
			printf("clockdata: time %02u:%02u:%02u date %u-%s-%u (%s)\n",
				cd.hour, cd.min, cd.sec,
				cd.mday, _month(cd.month), cd.year, _wday(cd.wday)); 

			ULONG date = Date2Amiga(&cd);
#ifdef DEBUG
			_debug_amiga_date(date);
#endif
			
			if (argc > 0)
			{
				struct RDArgs *rdargs = ReadArgs(template, (LONG *)arg_array, NULL);
					
				if (rdargs != NULL)
				{
					if (arg_array[0] != 0) // Load rtc into amiga
					{
						printf("load rtc to systemtime...\n");
						if (systime_set(date))
						{
							printf("done!\n");
						}
						else printf("error\n");
					}
					else if (arg_array[1] != 0) // Save amiga into rtc
					{
						printf("save current systemtime to rtc...\n");
						if (systime_get(&date))
						{
							_debug_amiga_date(date);
							Amiga2Date(date, &cd);

							printf("clockdata: time %02u:%02u:%02u date %u-%s-%u (%s)\n",
								cd.hour, cd.min, cd.sec,
								cd.mday, _month(cd.month), cd.year, _wday(cd.wday)); 
							
							if (_write_rtc(&cd))
							{
								printf("done!\n");
							}
							else printf("i2c port error\n");

						} else printf("couldn't get current systime\n");
					}
				} else printf("Use %s\n", template);
			}
		} else printf("i2c port error\n");
	
		CloseLibrary((struct Library *)UtilityBase);
	} else printf("couldn't open utility.library\n");
	
	return 0;
}

static void _debug_amiga_date(ULONG date)
{
#ifdef DEBUG
	printf("amiga date: %u\n", (unsigned)date); 
#endif

	struct DateTime dt;
	struct DateStamp *ds = &dt.dat_Stamp;
	ds->ds_Days = _idiv(&date, 60 * 60 * 24);
	ds->ds_Minute = _idiv(&date, 60);
	ds->ds_Tick = date * TICKS_PER_SECOND;
#ifdef DEBUG
	printf("datestamp: d/m/t %ld/%ld/%ld\n", 
		ds->ds_Days, ds->ds_Minute, ds->ds_Tick), 
#endif

	dt.dat_Format = FORMAT_DOS;
	dt.dat_Flags = 0;
	dt.dat_StrDay = _strday;
	dt.dat_StrDate = _strdate;
	dt.dat_StrTime = _strtime;
	if (DateToStr(&dt))
	{
		printf("amigados: %s %s (%s)\n", _strtime, _strdate, _strday);
	} else printf("DateToStr() failed!\n");				
}


static bool _read_rtc(struct ClockData *cd)
{
	i2c_context_t i2c;

	if (hal_i2c_initialize(0, 200000UL))
	{
		unsigned char cmd[] = { 0x00 };	// reg pointer

		hal_i2c_init_context(&i2c, DS3231_ADDR, cmd, sizeof(cmd));
		bool done = hal_i2c_read(0, &i2c, 
			_rtc_data, sizeof(_rtc_data));
		if (done)
		{
			cd->sec = (((_rtc_data[0] & 0x70) >> 4) * 10) +
				(_rtc_data[0] & 0xf); 
			cd->min = (((_rtc_data[1] & 0x70) >> 4) * 10) +
				(_rtc_data[1] & 0xf); 
			cd->hour = (((_rtc_data[2] & 0x70) >> 4) * 10) +
				(_rtc_data[2] & 0xf); 

			cd->mday = (((_rtc_data[4] & 0x30) >> 4) * 10) +
				(_rtc_data[4] & 0xf);
			cd->month = (((_rtc_data[5] & 0x10) >> 4) * 10) +
				(_rtc_data[5] & 0xf);
			cd->year = ((_rtc_data[6] >> 4) * 10) +
				(_rtc_data[6] & 0xf) + 2000;
			if (_rtc_data[5] & 0x80) cd->year -= 100;

			cd->wday = (_rtc_data[3] & 0xf) - 1; 

			return true;
		} else printf("i2c read error!\n");
	} else printf("i2c init failed\n");
	return false;
}

static bool _write_rtc(struct ClockData *cd)
{
	i2c_context_t i2c;

	if (hal_i2c_initialize(0, 200000UL))
	{
		unsigned char cmd[] = { 0x00 };	// reg pointer

		hal_i2c_init_context(&i2c, DS3231_ADDR, cmd, sizeof(cmd));

		_rtc_data[0] = ((cd->sec / 10) << 4) | (cd->sec % 10);
		_rtc_data[1] = ((cd->min / 10) << 4) | (cd->min % 10);
		_rtc_data[2] = ((cd->hour / 10) << 4) | (cd->hour % 10);

		_rtc_data[3] = cd->wday + 1;

		_rtc_data[4] = ((cd->mday / 10) << 4) | (cd->mday % 10);
		_rtc_data[5] = ((cd->month / 10) << 4) | (cd->month % 10);
		
		unsigned yc = (cd->year >= 2000) ? cd->year - 2000 : cd->year - 1900;
		if (cd->year < 2000) _rtc_data[5] |= 0x80;
		_rtc_data[6] = ((yc / 10) << 4) | (yc % 10);
		
		bool done = hal_i2c_write(0, &i2c, 
			_rtc_data, sizeof(_rtc_data));
		if (done)
		{
			return true;
		} else printf("i2c write error!\n");
	} else printf("i2c init failed\n");
	return false;
}

static const char *_month(unsigned month)
{
	switch(month)
	{
		case 1:	return "jan";
		case 2:	return "feb";
		case 3:	return "mar";
		case 4:	return "apr";
		case 5:	return "may";
		case 6:	return "jun";
		case 7:	return "jul";
		case 8:	return "aug";
		case 9:	return "sep";
		case 10:	return "oct";
		case 11:	return "nov";
		case 12:	return "dec";
		default:	return "???";
	}
}

static const char *_wday(unsigned wday)
{
	switch(wday)
	{
		case 0:	return "sunday";
		case 1:	return "monday";
		case 2:	return "tuesday";
		case 3:	return "wednesday";
		case 4:	return "thursday";
		case 5:	return "friday";
		case 6:	return "saturday";
		default:	return "???";
	}
}

static unsigned _idiv(ULONG *ap, ULONG b)
{
	unsigned a = *ap;
	unsigned q = a / b;
	*ap = a - (b * q);
	return q;
}



